!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!--------------------------------------------------------------------
SUBROUTINE lc_stop( full_run  )
  !----------------------------------------------------------------------------
  !
  ! This subroutine synchronizes processes before stopping.
  !
  USE kinds,                ONLY : DP
  USE mp_global,            ONLY : mp_global_end
  USE lc_variables,         ONLY : code1
  USE io_global,            ONLY : ionode
  USE io_files,             ONLY : tmp_dir, prefix
  USE io_global,            ONLY : stdout
  USE environment,          ONLY : environment_end
  USE lsda_mod,             ONLY : nspin
  USE noncollin_module,     ONLY : noncolin
  USE ions_base,            ONLY : nat, ityp, atm, ntyp => nsp, tau
  USE cell_base,            ONLY : celldm, at, bg, alat, omega
  USE klist,                ONLY : nelec
  !
  IMPLICIT NONE
  !
  LOGICAL, INTENT(IN) :: full_run
  CHARACTER(len=6), EXTERNAL :: int_to_char
  CHARACTER(len=256) :: filename
  INTEGER :: ip,i,j
  REAL(kind=dp) :: degspin
  !
  ! Deallocate lc variables
  !
  CALL lc_dealloc()
  !
  CALL environment_end(code1) 
  !
  CALL mp_global_end( )
  !
#if defined (__T3E)
  !
  ! ... set streambuffers off
  !
  CALL set_d_stream( 0 )
  !
#endif
  !
  STOP
  !
END SUBROUTINE lc_stop
