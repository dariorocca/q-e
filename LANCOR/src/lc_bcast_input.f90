!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE lc_bcast_input
  !-----------------------------------------------------------------------
  !
  !  Broadcasting the input variables.
  !
#ifdef __MPI

  USE lc_variables
  USE mp,                  ONLY: mp_bcast, mp_barrier
  USE io_files,            ONLY: tmp_dir, prefix, wfc_dir
  USE control_flags,       ONLY: tqr, tddfpt
  USE io_global,           ONLY: ionode, ionode_id
  USE mp_global,           ONLY: intra_image_comm
  USE mp_world,            ONLY: world_comm
  USE qpoint,              ONLY: xq
  USE input_parameters,    ONLY: outdir

  IMPLICIT NONE
  !
  CALL mp_barrier(world_comm)
  CALL mp_bcast (niterlan, ionode_id, world_comm )
  CALL mp_bcast (prefix, ionode_id, world_comm )
  CALL mp_bcast (tmp_dir, ionode_id, world_comm )

  CALL mp_bcast (wfc_dir, ionode_id, world_comm )
  CALL mp_bcast (naux, ionode_id, world_comm )
  CALL mp_bcast (rpa_only, ionode_id, world_comm )
  CALL mp_bcast (nfreq, ionode_id, world_comm )
  CALL mp_barrier(world_comm)

#endif
  RETURN
END SUBROUTINE lc_bcast_input
