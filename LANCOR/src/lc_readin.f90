!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE lc_readin
  !-----------------------------------------------------------------------
  !
  !    This routine reads the control variables from standard input (unit 5).
  !    A second routine read_file reads the variables saved to file
  !    by PWscf.
  !
  USE lc_variables
  USE kinds,               ONLY : DP
  USE io_files,            ONLY : tmp_dir, prefix, wfc_dir
  USE lsda_mod,            ONLY : current_spin, nspin, isk, lsda
  USE control_flags,       ONLY : use_para_diag, &
                                  & tqr, gamma_only, &
                                  & do_makov_payne
  USE scf,                 ONLY : vltot, v, vrs, vnew, &
                                  & destroy_scf_type, rho
  USE fft_base,            ONLY : dfftp, dffts
  USE gvecs,               ONLY : doublegrid
  USE wvfct,               ONLY : nbnd, et, wg, current_k
  USE lsda_mod,            ONLY : isk
  USE ener,                ONLY : ef
  USE io_global,           ONLY : ionode, ionode_id, stdout
  USE klist,               ONLY : nks, wk, nelec, lgauss, ltetra
  USE fixed_occ,           ONLY : tfixed_occ
  USE xc_lib,              ONLY : xclib_dft_is
  USE input_parameters,    ONLY : degauss, nosym, wfcdir, outdir,&
                                  & max_seconds
  USE realus,              ONLY : real_space, &
                                  & init_realspace_vars, qpointlist,&
                                  & betapointlist
  USE mp,                  ONLY : mp_bcast
  USE mp_world,            ONLY : world_comm
  USE mp_global,           ONLY : my_pool_id, intra_image_comm, &
                                  & intra_bgrp_comm, nproc_image, &
                                  & nproc_pool, nproc_pool_file, &
                                  & nproc_image_file, nproc_bgrp, &
                                  & nproc_bgrp_file, my_image_id
  USE DFUNCT,              ONLY : newd
  USE vlocal,              ONLY : strf
  USE martyna_tuckerman,   ONLY : do_comp_mt
  USE esm,                 ONLY : do_comp_esm
  USE qpoint,              ONLY : xq
  USE noncollin_module,    ONLY : noncolin
  USE mp_bands,            ONLY : ntask_groups
  USE constants,           ONLY : eps4
  USE ldaU,                ONLY : lda_plus_u
  USE control_lr,          ONLY : nbnd_occ
  

  IMPLICIT NONE
  !
  CHARACTER(LEN=256), EXTERNAL :: trimcheck
  !
  CHARACTER(LEN=256) :: beta_gamma_z_prefix
  ! Fine control of beta_gamma_z file
  CHARACTER(LEN=80) :: disk_io
  ! Specify the amount of I/O activities
  INTEGER :: ios, iunout, ierr, ipol
  LOGICAL :: auto_rs
  CHARACTER(LEN=6) :: int_to_char
  !
  NAMELIST / lc_input /   prefix, outdir, niterlan, naux, rpa_only, nfreq
  !
#ifdef __MPI
  IF (ionode) THEN
#endif
     !
     ! Checking for the path to the output directory.
     !
     CALL get_environment_variable( 'ESPRESSO_TMPDIR', outdir )
     IF ( trim( outdir ) == ' ' ) outdir = './'
     !
     ! Set default values for variables in namelist.
     !
     prefix='pwscf'
     outdir='./'
     niterlan=30
     naux=10
     nfreq=12
     rpa_only=.true.
     !
     !   Reading the namelist lc_input
     !
     CALL input_from_file( )
     !
     READ (5, lc_input, err = 200, iostat = ios)
200  CALL errore ('lc_readin', 'reading lc_input namelist', ABS (ios) )
     !
     ! Set-up all the dir and suffix variables.
     !
     outdir = trimcheck(outdir)
     tmp_dir = outdir
     !
     ! The LANCOR code is limited to NC pseudopotentials and gamma_only.
     !
     CALL input_sanity()
     !
     ierr = 0
     !
     !
#ifdef __MPI
  ENDIF
  !
  CALL lc_bcast_input
#endif
  !
  outdir = TRIM( tmp_dir ) // TRIM( prefix ) // '.save'
  !
  ! Now PWSCF XML file will be read, and various initialisations will be done.
  !
  CALL read_file()
  !
  !
  wfc_dir = trimcheck ( wfcdir )
  !
  !  Deallocate some variables created with read_file but not used in LANCOR
  !
  DEALLOCATE( strf )
  CALL destroy_scf_type(vnew)
  !
  ! Now put the potential calculated in read_file into the correct place
  ! and deallocate the redundant associated variables.
  ! Set the total local potential vrs on the smooth mesh 
  ! adding the scf (Hartree + XC) part and the sum of 
  ! all the local pseudopotential contributions.
  ! vrs = vltot + v%of_r
  !
  CALL set_vrs ( vrs, vltot, v%of_r, 0, 0, dfftp%nnr, nspin, doublegrid )
  !
  DEALLOCATE( vltot )
  CALL destroy_scf_type(v)
  !
  ! Recalculate the weights of the Kohn-Sham orbitals.
  !
  CALL iweights( nks, wk, nbnd, nelec, et, ef, wg, 0, isk)
  !
  RETURN
  !
CONTAINS
  !
  SUBROUTINE input_sanity()
    !-------------------------------------------------------------------------- 
    ! 
    ! This subroutine aims to gather all of the input sanity checks
    ! for the LANCOR code
    !
    USE paw_variables,    ONLY : okpaw
    USE uspp,             ONLY : okvan
    USE xc_lib,           ONLY : xclib_dft_is

    IMPLICIT NONE
    !
    !
    !  Meta-DFT currently not supported by LANCOR
    !
    IF (xclib_dft_is('meta')) &
         & CALL errore( 'lc_readin', ' Meta DFT ' // 'is not implemented yet', 1 )
    !
    !  Tetrahedron method and fixed occupations are not implemented.
    !
    IF (ltetra)                 CALL errore( 'lc_readin', 'ltetra is not implemented', 1 )
    IF (tfixed_occ)             CALL errore( 'lc_readin', 'tfixed_occ is not implemented', 1 )
    !
    !  Metals not supported.
    !
    IF (lgauss) CALL errore( 'lc_readin', 'the method is not extended to metals', 1 )
    !
    ! K-points not implemented in this version
    !
    IF (.NOT. gamma_only ) CALL errore('lc_readin', 'k-points not implemented',1)
    !
    ! No taskgroups 
    !
    IF (dffts%has_task_groups) &
         & CALL errore( 'lc_readin', 'No Task groups implementation', 1 )
    !
    ! No PAW support.
    !
    IF (okpaw) &
         & CALL errore( 'lc_readin', 'PAW not supported', 1 )
    !
    IF (okvan .AND. xclib_dft_is('hybrid')) &
         & CALL errore( 'lc_readin', 'US not supported', 1 )
    !
    ! Spin-polarised case is not implemented, but partially accounted in
    ! some routines.
    !
    IF (lsda) CALL errore( 'lc_readin', 'LSDA is not implemented', 1 )
    !
    ! Hubbard U is not supported
    !
    IF (lda_plus_u) CALL errore('lr_readin', 'TDDFPT with Hubbard U is not implemented',1)
    !
    ! Hybrid functionals are not supported
    !
    IF (xclib_dft_is('hybrid'))    CALL errore( 'lc_readin', 'EXX is not supported', 1 )
    !
    RETURN
    !
  END SUBROUTINE input_sanity
  !
  !------------------------------------------------------------------------
END SUBROUTINE lc_readin
