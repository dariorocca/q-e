!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE lc_dealloc()
  !---------------------------------------------------------------------
  !
  ! This subroutine deallocates all the Lanczos variables.
  !
  USE lc_variables
  USE uspp,           ONLY : nkb
  USE control_flags,  ONLY : gamma_only
  USE io_global,      ONLY : stdout
  USE qpoint,         ONLY : ikks, ikqs, igkq, eigqts
  USE eqv,            ONLY : dmuxc, evq, dpsi, dvpsi
  USE becmod,         ONLY : bec_type, becp, deallocate_bec_type
  USE control_lr,     ONLY : nbnd_occ
  !
  IMPLICIT NONE
  !
  INTEGER :: ik
  !
  IF (ALLOCATED(evc0))      DEALLOCATE(evc0)
  IF (ALLOCATED(d0psi))     DEALLOCATE(d0psi)
  if (ALLOCATED(revc0))     DEALLOCATE(revc0)
  !
  IF (ALLOCATED(nbnd_occ)) DEALLOCATE(nbnd_occ)
  !
  IF (gamma_only) THEN
     CALL lc_dealloc_gamma()
  ELSE
!     CALL lr_dealloc_k()
  ENDIF
  !
  RETURN
  !
CONTAINS
  !
  SUBROUTINE lc_dealloc_gamma()
    !
    IF (nkb > 0) THEN
       !
       CALL deallocate_bec_type(becp)
       !
!       DEALLOCATE(becp_1)
       !
    ENDIF
    !
    RETURN
    !
  END SUBROUTINE lc_dealloc_gamma
  !
END SUBROUTINE lc_dealloc
!-----------------------------------------------------------------------
