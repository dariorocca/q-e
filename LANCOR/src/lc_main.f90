!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
PROGRAM lc_main
  !---------------------------------------------------------------------
  !
  ! This is the main driver of the Lanczos Correlation code
  ! Author: Dario Rocca
  !
  USE lc_variables,          ONLY : code1, naux_from_file, eigenval_eps, iun_alpha_beta, iun_zeta
  USE io_global,             ONLY : stdout
  USE kinds,                 ONLY : dp
  USE lc_variables,          ONLY : niterlan, naux, norm0, revc0 
  USE lc_variables,          ONLY : evc0, veh, d0psi, rpa_only
  USE io_files,              ONLY : nd_nmbr, diropn
  USE klist,                 ONLY : ngk, igk_k
  USE global_version,        ONLY : version_number
  USE ions_base,             ONLY : tau,nat,atm,ityp
  USE environment,           ONLY : environment_start
  USE mp_global,             ONLY : nimage, mp_startup, &
                                    ibnd_start, ibnd_end
  USE wvfct,                 ONLY : nbnd, npwx
  USE wavefunctions,         ONLY : psic
  USE fft_interfaces,        ONLY : invfft, fwfft
  USE check_stop,            ONLY : check_stop_now, check_stop_init
  USE fft_base,              ONLY : dffts
!  USE gvecs,                 ONLY : nls, nlsm
  USE mp_bands,              ONLY : ntask_groups
  USE control_flags,         ONLY : tddfpt 
  USE cell_base,             ONLY : tpiba2
  USE gvect,                 ONLY : g
  USE constants,             ONLY : e2, fpi
  USE io_files,              ONLY : prefix
  USE io_global,             ONLY : ionode
  USE mp,                  ONLY: mp_bcast, mp_barrier
  USE io_global,           ONLY: ionode_id
  USE cell_base,            ONLY : omega
  USE mp_world,            ONLY: world_comm

  !
  IMPLICIT NONE
  !
  ! Local variables
  !
  INTEGER            :: ip,ip1,na,pol_index, nrxx, i
  INTEGER            :: iter_restart,iteration
  LOGICAL            :: rflag, tg_tmp
  COMPLEX(kind=dp)   :: temp
  LOGICAL, EXTERNAL  :: test_restart
  integer            :: ik, ibnd, ibnd1, ig, igg, iter, ig1, nword_veh
  REAL(DP) :: qg, qg2, fac, minv, sum_pj
  complex(kind=dp), allocatable :: veh_temp(:)
  logical :: exst, rstart_lan
  real(dp) :: pj
  INTEGER  :: iunwfc, stat, stat1, stat2, stat3, naux_restart, niterlan_restart, ip_restart, naux_read
  INTEGER, EXTERNAL :: find_free_unit
  COMPLEX(kind=dp), ALLOCATABLE :: d0psi_pol(:,:,:,:)
  !
  pol_index = 1
  !
#ifdef __MPI
  CALL mp_startup ( )
#endif
  !
  CALL environment_start ( code1 )
  !
  CALL start_clock('lc_main')
  !
  ! Let the PHonon routines know that 
  ! they are doing tddfpt.
  !
  tddfpt = .TRUE.
  !
  ! Reading input file and PWSCF xml, some initialisation;
  ! Read the input variables for Lanczos correlation (LANCOR);
  ! Allocate space for all quantities already computed
  ! by PWscf, and read them from the data file;
  ! Define the tmp_dir directory.
  !
  CALL lc_readin()
  !
  !
  CALL check_stop_init()
  !
  ! Initialisation
  !
  CALL lc_init_nfo()
  !
  ! Allocate the arrays
  !
  CALL lc_alloc_init()
  !
  ! Print a preamble info about the run
  !
  CALL lc_print_preamble()
  !
  IF ( ntask_groups > 1 ) WRITE(stdout,'(5X,"Task groups is activated...")' )
  !
  ! Read ground state wavefunctions from PWscf.
  !
  CALL lc_read_wf()
  !
  !
  write(stdout,*)
  WRITE(stdout,'(/,5X,"LANCZOS CORRELATION CALCULATION")')
  WRITE(stdout,'(5X," ")')
  WRITE(stdout,'(5x,"Number of Lanczos iterations = ",i6)') niterlan
  write(stdout,*)
  !
  ! Lanczos loop to compute the correlation energy
  !
!  IF (.not.lanczos) THEN   ! Computing the auxiliary basis set
     !
     ALLOCATE(veh(npwx,naux))
     veh=(0.D0,0.D0)
     !
     ALLOCATE(eigenval_eps(naux))
     eigenval_eps=0.d0
     !
     write(stdout,'(/5x,"!!! STEP 1: AUXILIARY BASIS SET GENERATION !!!")')
     write(stdout,*)
     !
     !!!!!!!!!!!!!!!!!!!!! CODE FOR RESTART !!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     ! Checking if restart file exist and reading eigenvalues 
     !
     IF (ionode) open(unit=158, iostat=stat, file="restart_aux", status='old')
     !
     IF (stat == 0 .and. ionode) THEN
        !
        READ(158,'(i6)') naux_from_file 
        IF (naux.le.naux_from_file) THEN
           naux_read=naux
        ELSE
           naux_read=naux_from_file
        END IF
        !
        DO ig=1,naux_read
           !
           READ(158,*) eigenval_eps(ig)
           !
        END DO
        !
     ELSE
        !
        ! No restart file available
        !
        naux_from_file=0
        !
     END IF
     !
     IF (ionode) close(158)
     !
#ifdef __MPI
     CALL mp_barrier(world_comm)
     CALL mp_bcast (naux_from_file, ionode_id, world_comm )
     CALL mp_bcast (eigenval_eps, ionode_id, world_comm )
     CALL mp_barrier(world_comm)
#endif
     !
     IF (naux>naux_from_file .and. naux_from_file.ne.0) THEN
        !
        ! When restarting the eigenvectors already computed are read from file
        !
        WRITE(stdout,'(5x,"RESTART:",i5," auxiliary basis vectors already computed")') naux_from_file
        !
        iunwfc = find_free_unit()
        nword_veh = 2 * npwx
        !
        call diropn ( iunwfc, 'veh', nword_veh, exst)
        IF (.not.exst) call errore('lc_read_wfc', TRIM( prefix )//'.veh'//' not found',1)
        !
        DO ig=1,naux_from_file
           call davcio(veh(1,ig),nword_veh,iunwfc,ig,-1)
        END DO
        !
        close(iunwfc)
        !
     ELSE IF (naux.le.naux_from_file) then
        !
        WRITE(stdout,'(/,5X,"RESTART: Auxiliary basis set already computed")')
        WRITE(stdout,*)
        !
        DO ig = 1, naux
           write(stdout,*) "FINAL EIGENVALUE", eigenval_eps(ig)*4.d0/omega
        END DO
        !
     END IF
     !
     !!!!!!!!!!!!!!!!!!!!! END CODE FOR RESTART !!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     !
     IF (naux>naux_from_file) THEN
     !
     !!!!!!!!!!!!!!!!!!!!! COMPUTING EIGENVECTORS WITH CG ALGORITHM  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !       
     CALL lc_cg_solver( )
     !
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     ! Writing eigenvalues and eigenvectors to file
     !
     nword_veh = 2 * npwx
     ! 
     iunwfc = find_free_unit()
     call diropn ( iunwfc, 'veh', nword_veh, exst)
     !
     do ig=1,naux
        call davcio(veh(1,ig),nword_veh,iunwfc,ig,+1)
     end do
     !
     close(iunwfc)
     !
     if (ionode) then 
        open(unit=158, file="restart_aux", status='unknown')
        write (158,'(i6)') naux
        !
        do ig=1,naux
           write (158,*) eigenval_eps(ig)
        end do
        !
        close(158)
     end if 
     END IF
     !
     deallocate(veh,eigenval_eps)
     !
     if (.true.) then
!  ELSE  ! Computing matrix elements with Lanczos algorithm
     write(stdout,*)
     write(stdout,*)
     write(stdout,'(/5x,"!!! STEP 2: LANCZOS COMPUTATION !!!")')
     write(stdout,*)
     write(stdout,*)
     !
     !!!!!!!!!!!!!!!!!!!!! COMPUTING MACROSCOPIC CONTRIBUTION !!!!!!!!!!!!!!!!!!!!!!!!! 
     !
!     ALLOCATE(d0psi_pol(npwx,nbnd,1,3))
     !
!     DO ip=1,3
!        CALL lr_dvpsi_e(1,ip,d0psi(:,:,1,ip))
!     ENDDO
     !
     !!!!!!!!!!!!!!!!!!!!! READING THE AUXILIARY BASIS SET !!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     nrxx = dffts%nnr
     allocate(veh_temp(nrxx))
     write(stdout,'(/5x,"Allocating d0psi",1x)') 
     allocate(d0psi(npwx,nbnd,1,naux))
     write(stdout,'(/5x,"Done",1x)')
     allocate(veh(npwx,1))
     !
     nword_veh = 2 * npwx
     ! 
     iunwfc = find_free_unit()
     call diropn ( iunwfc, 'veh', nword_veh, exst)
     if (.not.exst) call errore('lc_read_wfc', TRIM( prefix )//'.veh'//' not found',1)
     !
     do ig=1,naux
        !
        veh_temp(:)=(0.D0,0.D0)
        call davcio(veh(1,1),nword_veh,iunwfc,ig,-1)
        !
        do ig1 = 1, ngk(1)
           !  
           qg2 = (g(1,ig1))**2 + (g(2,ig1))**2 + (g(3,ig1))**2
           IF (qg2 > 1.d-8) fac = (e2*fpi)/(qg2*tpiba2)
           IF (qg2 > 1.d-8) veh (ig1,1) = veh(ig1,1) * sqrt(fac)
           !  
        end do
        !
        do ig1 = 1, ngk(1)
           !
           veh_temp(dffts%nl(ig1))=veh(ig1,1)
           veh_temp(dffts%nlm(ig1))=conjg(veh(ig1,1))
           !
        end do
        ! 
        CALL invfft ('Wave', veh_temp, dffts)
        !
        do ibnd=1,nbnd
           psic(:)=veh_temp(:)*revc0(:,ibnd,1)
           CALL fwfft ('Wave', psic, dffts)
           do igg=1,ngk(1)
              d0psi(igg,ibnd,1,ig)=psic(dffts%nl(igk_k(igg,1)))
           end do
        end do
        !
        CALL orthogonalize(d0psi(:,:,1,ig), evc0(:,:,1), &
                 & 1, 1, evc0(:,:,1), ngk(1), .true.)
        d0psi(:,:,1,ig)=-d0psi(:,:,1,ig)
        !
     end do

     close(iunwfc)

     deallocate(veh_temp,veh)

     !!!!!!!!!!!!!!!!!!!!! END READING THE AUXILIARY BASIS SET !!!!!!!!!!!!!!!!!!!!!!!!!

     !!!!!!!!!!!!!!!!!!!!! READING LANCZOS RESTART FILES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

       if (ionode .and. rpa_only) then
           !
           open(unit=159, iostat=stat, file="restart_lan", status='old')
           !
           if (stat == 0) then
              !
              read(159,*) ip_restart
              read(159,*) niterlan_restart
              read(159,*) naux_restart
              rstart_lan=.true.
              if (niterlan_restart.ne.niterlan .or. naux_restart.ne.naux) then
                 write(stdout,'(/5x,"!!! naux or niterlan have been changed !!!")')
                 write(stdout,'(/5x,"!!! Restarting Lanczos calculation from scratch !!!")')
                 rstart_lan=.false.
              end if
              !
           end if
           !
           if (rstart_lan) then
              !     
              open(unit=iun_alpha_beta, iostat=stat2, file="alpha_beta_store", status='old')
              !
              if (stat2 == 0) then
                 !
                 do ip=1,ip_restart
                    !
                    read(iun_alpha_beta,*) 
                    !
                    do i=1,niterlan
                       !
                       read(iun_alpha_beta,*) 
                       read(iun_alpha_beta,*)
                       !
                    end do
                    !
                 end do
                 !
              else
                 !
                 call errore('lc_main', 'alpha_beta_store file not found',1) 
                 !
              end if
              !
              open(unit=iun_zeta, iostat=stat3, file="zeta_store", status='old')
              !
              if (stat3 == 0) then
                 !
                 do ip=1,ip_restart
                 do ip1=1,naux
                    !
                    do i=1,niterlan
                       !
                       read(iun_zeta,*)   
                       !
                    end do
                    !
                 end do
                 end do
                 !           
              else
                 !
                 call errore('lc_main', 'zeta_store file not found',1)
                 !
              end if
              !             
           else
              !
              open(unit=158, iostat=stat1, file="alpha_beta_store", status='old')
              if (stat1 == 0) close(158, status='delete')
              !
              open(unit=158, iostat=stat1, file="zeta_store", status='old')
              if (stat1 == 0) close(158, status='delete')
              !
              open(unit=iun_alpha_beta, iostat=stat2, file="alpha_beta_store", status='unknown')
              open(unit=iun_zeta, iostat=stat3, file="zeta_store", status='unknown')
              ip_restart=0
              !
           end if
           !
           close(159)
           !
       end if
       !
#ifdef __MPI
       CALL mp_barrier(world_comm)
       CALL mp_bcast (ip_restart, ionode_id, world_comm )
       CALL mp_barrier(world_comm)
#endif

     !!!!!!!!!!!!!!!!!!!!! END READING LANCZOS RESTART FILES !!!!!!!!!!!!!!!!!!!!!!!!!!!!

     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  LANCZOS LOOP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     ! In the rpa_only case only the RPA correlation energy is computed (faster)
     ! In the general case the RPA, ACSOSEX, and eh-TDHF correlation energies
     ! are computed.
     !
     IF (rpa_only) THEN
        !
        DO ip=ip_restart+1, naux
           !
           write(stdout,'(/5x,"Lanczos loop for auxiliary vector #",1x,i8)')   ip
           !
           call lc_lanczos_solver(niterlan,ip)
           !
           if (ionode) then
              open(unit=159, file="restart_lan", status='unknown')
                  write(159,*) ip-1
                  write(159,*) niterlan
                  write(159,*) naux
              close(159)
           end if
           !
        END DO
        !
        if (ionode) CLOSE(iun_alpha_beta)
        if (ionode) CLOSE(iun_zeta)
        !
     ELSE
        !
        if (ionode) open(unit=158, iostat=stat1, file="zeta_store", status='old')
        if (stat1 == 0 .and. ionode) close(158, status='delete')
        ! 
        DO ip=1, naux
           !
           write(stdout,'(/5x,"Lanczos loop for auxiliary vector #",1x,i8)')   ip
           !
           call lc_lanczos_solver_v(niterlan,ip)
           !
        END DO
        !
     END IF
     !
     !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! END  LANCZOS LOOP !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     !
     !!!!!!!!!!!!!!!!!!!!!! COMPUTING CORRELATION ENERGY  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  END IF
 end if
  !
  write(stdout,*)
  write(stdout,*)
  write(stdout,'(/5x,"!!! STEP 3: CORRELATION ENERGY COMPUTATION !!!")')
  write(stdout,*)
  write(stdout,*)
  CALL lc_compute_rpa( )
  ! 
  ! wrapping up
  !
  CALL clean_pw( .FALSE. )
  CALL stop_clock('lc_main')
  CALL lc_print_clock()
  CALL lc_stop( .FALSE. )
  !
  WRITE(stdout,'(5x,"End of Lanczos iterations")')
  !
  ! Deallocate PW variables
  !
  CALL clean_pw( .FALSE. )
  !
  WRITE(stdout,'(5x,"Finished Lanczos correlation calculation...")')
  !
  CALL stop_clock('lc_main')
  !
  CALL lc_print_clock()
  !
  CALL lc_stop( .TRUE. )

CONTAINS
 
SUBROUTINE lc_print_preamble()
    
    USE uspp,                ONLY : okvan
    USE martyna_tuckerman,   ONLY : do_comp_mt
    USE control_flags,       ONLY : do_makov_payne
    USE lc_variables,        ONLY : rpa_only
    !
    IMPLICIT NONE
    !
    WRITE( stdout, '(/5x,"----------------------------------------")' )
    WRITE( stdout, '(/5x,"")' )
    WRITE( stdout, '(/5x,"Lanczos correlation (LANCOR)")' )
    IF (.NOT.rpa_only) WRITE( stdout, '(/5x,"Computing RPA, AC-SOSEX, and eh-TDHF ground-state correlation energies")' )
    IF (rpa_only) WRITE( stdout, '(/5x,"Computing RPA ground-state correlation energies")' )
    !
    RETURN
    !
END SUBROUTINE lc_print_preamble

END PROGRAM lc_main
!-----------------------------------------------------------------------
