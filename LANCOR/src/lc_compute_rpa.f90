!-----------------------------------------------------------------------
subroutine lc_compute_rpa
  !---------------------------------------------------------------------
  ! ... calculates the RPA correlation energy (rpa_only)
  ! ... by solving tridiagonal problem for each value of omega
  !---------------------------------------------------------------------
  !
  USE lc_variables,          ONLY : iun_alpha_beta, iun_zeta, niterlan, nfreq, naux
  USE io_global,             ONLY : ionode
  USE cell_base,             ONLY : omega
  !
  implicit none
  !
  integer, parameter :: dp=kind(0.D0)
  integer :: i, j, ip, ip2, ipg, ipq, ipp, ibnd, jbnd, counter, val_dim, max_loc(2), max_1, max_2
  real(kind=dp) :: omeg, omegmax, delta_omeg, sum_correl, sum_correl_old, sum_correl_old2
  real(kind=dp), allocatable :: value(:), valuesosex(:), valuerpx(:), valuemp2(:) 
  real(kind=dp), allocatable :: green(:,:)
  real(kind=dp), allocatable :: grr(:,:),grr2(:,:),Y(:,:),Yii(:)
  complex(kind=dp), allocatable :: a(:), b(:), c(:), r_a(:)
  integer :: ipo1, ipo2, ipo3, ipo4, maxo(1)
  real(kind=dp), allocatable :: Y2(:,:,:,:)
  !
  complex(kind=dp), external :: zdotc
  real(kind=dp), external ::ddot
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer :: ios
  real(kind=dp) :: zeta, alpha, beta
  real(kind=dp), allocatable :: &
       alpha_store(:,:),&
       beta_store(:,:)
  REAL(DP), ALLOCATABLE :: u_ec(:), wu_ec(:)
  real(kind=dp), allocatable :: zeta_store(:,:)
  complex(kind=dp), allocatable :: zeta_temp(:)
  integer :: niterlan_read
  integer :: ipol
  integer :: itemp
  REAL(kind=dp), allocatable :: norm0(:)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  integer :: lwork,liwork,ippo2,info
  real(kind=dp),allocatable :: eval1(:), eval2(:)
  real(kind=dp), allocatable :: work(:)
  integer, allocatable :: iwork(:)
  integer :: stat2, step=1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  niterlan_read = niterlan
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  IF (ionode) THEN
  !
  allocate(norm0(naux))
  !
  !
  allocate(alpha_store(naux,niterlan))
  allocate(beta_store(naux,niterlan))
  allocate(zeta_store(naux,niterlan))
  allocate(zeta_temp(niterlan))
  alpha_store(:,:)=0.0d0
  beta_store(:,:)=0.0d0
  zeta_store(:,:)=0.0d0
  zeta_temp(:)=(0.0d0,0.0d0)
  !
  allocate( u_ec(nfreq), wu_ec(nfreq) )
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  omegmax=0.0d0
  delta_omeg=0.01d0
  omeg=0.0d0
  !
  allocate(a(niterlan))
  allocate(b(niterlan-1))
  allocate(c(niterlan-1))
  allocate(r_a(niterlan))
  allocate(green(naux,naux))
  !
  a(:) = (0.0d0,0.0d0)
  b(:) = (0.0d0,0.0d0)
  c(:) = (0.0d0,0.0d0)
  r_a(:) = (0.0d0,0.0d0)
  !
  val_dim=naux/step
  allocate(value(val_dim))
  value(:)=0.D0
  !
  sum_correl=0.d0
  !
  call tranform_freq (nfreq, u_ec, wu_ec)
  !
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  open(unit=iun_alpha_beta, iostat=stat2, file="alpha_beta_store", status='old')
  !
  if (stat2 == 0) then
     !
     do ip=1,naux
        !
        read(iun_alpha_beta,'(38X,d24.16)') norm0(ip)
        !
        do i=1,niterlan
           !
           read(iun_alpha_beta,'(12X,i8.8,2X,d24.16)') itemp, alpha_store(ip,i)
           read(iun_alpha_beta,'(11X,i8.8,2X,d24.16)') itemp, beta_store(ip,i)
           !
        end do
        !
     end do
     !
     else
     !
     call errore('lc_compute_rpa', 'alpha_beta_store file not found',1)
     !
  end if
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  open (158, file = "zeta_store", form = 'formatted', status = 'old')
  !
  !
  do counter=1,nfreq 
     !
!u_ec(counter)=0.D0
     rewind(158)
     !
     green=0.D0
     !
     do ipp=1, naux
        !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        do ibnd=1,nbnd
!           read(158,'(d)') et(ibnd) 
!        end do
        !
        do i=1,niterlan_read
           do ipq=1,naux
              !
              read(158,'(d24.16)') zeta
              if (i.le.niterlan) zeta_store(ipq,i)=zeta
              !
           end do
        end do
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !
           !
           do i=1,(niterlan-1)
              !
              a(i) = cmplx(-alpha_store(ipp,i),u_ec(counter),dp)
              b(i)=cmplx(-beta_store(ipp,i),0.0d0,dp)
              c(i)=cmplx(-beta_store(ipp,i),0.0d0,dp)
              !
           end do
           a(niterlan)=cmplx(-alpha_store(ipp,niterlan),u_ec(counter),dp)
           !
           r_a(:) =(0.0d0,0.0d0)
           r_a(1)=(1.0d0,0.0d0)
           !
           call zgtsv(niterlan,1,b,a,c,r_a(:),niterlan,info)
           if(info /= 0) write(*,*) " unable to solve tridiagonal system 1"
           !
           do ipq=1,naux
              !
              zeta_temp(:)=cmplx(zeta_store(ipq,:),0.d0,dp)
              green(ipq,ipp)=green(ipq,ipp)+norm0(ipp)*DDOT(2*niterlan,zeta_temp(:),1,r_a(:),1)*(4.D0/omega)
              !
           end do
           !
        !
        !
     end do
     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!write(*,*) "CHECKING SYMMETRY OF DIELECTRIC MATRIX" 
!max_loc=maxloc(abs(green-transpose(green)))
!max_1=max_loc(1)
!max_2=max_loc(2)
!write(*,*) "Max deviation and corr. % (d)",abs(green(max_1,max_2)-green(max_2,max_1)),(abs(green(max_1,max_2)-green(max_2,max_1))/abs(green(max_1,max_2)))*100.d0
     
     do ip=1,val_dim
        ip2=ip*step
        !
        allocate(eval1(ip2))
        eval1(:)=0.d0
        allocate(eval2(ip2))
        eval2(:)=0.d0
        !
        lwork=2*ip2**2+6*ip2+1
        allocate(work(lwork))
        work=0.D0
        !
        liwork=5*ip2+3
        allocate(iwork(liwork))
        iwork=0
        !
        info=0
        !
        allocate(grr(ip2,ip2))
        grr=0.d0
        do i=1,ip2
           do j=1,ip2
              grr(i,j)=green(i,j)
           end do
        end do
        !
        call dsyevd('v','u',ip2,grr,ip2,eval1,work,lwork,iwork,liwork,info)
        deallocate(work)
        deallocate(iwork)
        deallocate(grr)
        !
        if (info/=0) then
           write(*,*) "Diagonalisation unsuccessful"
           stop
        end if
        !
        !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!        write(*,'(5X,"# < start of eigenvalue listing >")')
!        do i=1,ip2
!           write(*,'(5X,i5,2(1X,f20.12))') i,eval1(i)
!        enddo
!        write(*,'(5X,"# < end of eigenvalue listing >")')
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !
        do ipo1=1,ip2
           if (abs(eval1(ipo1))>1.d-2) value(ip)=value(ip)+ wu_ec(counter) * ( log(1.d0-eval1(ipo1)) + eval1(ipo1) )
           if (abs(eval1(ipo1))<1.d-2) value(ip)=value(ip)- wu_ec(counter) * &
                  &  ( eval1(ipo1)**2/2.D0 + eval1(ipo1)**3/3.D0 + eval1(ipo1)**4/4.D0 + eval1(ipo1)**5/5.D0 &
                  &  + eval1(ipo1)**6/6.D0 + eval1(ipo1)**7/7.D0 + eval1(ipo1)**8/8.D0 )
        end do
        ! value(ip)=value(ip)+ wu_ec(counter) * sum( log(1.d0-eval1(:)) + eval1(:) )
        deallocate(eval1,eval2)
        !
        if (counter==nfreq) then
           write(*,*) "N_aux = ", ip2,"  E_RPA = ", value(ip)/(2.d0*3.1415d0) 
        end if
        !
     end do
     !
     omeg=omeg+delta_omeg
     !
  enddo
  !
  deallocate(a)
  deallocate(b)
  deallocate(c)
  deallocate(r_a)
  deallocate(green)
  !
  END IF
  !
end subroutine lc_compute_rpa
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
     !
     subroutine tranform_freq (nf_ec, uu_ec, wwu_ec)
     !
     implicit none
     !
     integer, parameter :: DP=kind(0.D0)
     integer :: nf_ec
     real(DP) :: uu_ec(nf_ec), wwu_ec(nf_ec)
     real(DP) :: uu0, u0_ec
     REAL(DP), ALLOCATABLE :: tt_nk(:), ww_nk(:), tu_ec(:)
     !
     ! Transformation of frequency and coupling constant 
     !
     u0_ec=200.D0
     allocate( tt_nk(nf_ec), ww_nk(nf_ec), tu_ec(nf_ec) )
     call gl_weight (nf_ec,tt_nk,ww_nk)
!     allocate( tu_ec(nf_ec), u_ec(nf_ec), wu_ec(nf_ec) )
     !
     uu0 = u0_ec * (1.d0+tt_nk(1))/(1.d0-tt_nk(1))
     u0_ec = uu0
     !
     tu_ec(:) = tt_nk(:)
     wwu_ec(:) = ww_nk(:) * 2.d0*u0_ec/((1.d0+tu_ec(:))**2)
     uu_ec(:) = u0_ec*(1.d0-tu_ec(:)) / (1.d0+tu_ec(:))
     !
     deallocate(tt_nk, ww_nk, tu_ec)
     !
     return
     !
     end subroutine tranform_freq
     !
     !---------------------------------------------------------
!
!-----------------------------------------------------------------------------
   subroutine gl_weight( nfs_, t_nk, w_nk)
   !--------------------------------------------------------------------------
   !
   ! this routine gives abscissas and weight of frequencies integration in 
   ! Gauss-Legendre method  
   !
   implicit none
   !
   integer, parameter :: DP=kind(0.D0)
   integer :: i, nfs_
   REAL(DP) :: t_nk(nfs_), w_nk(nfs_)
   !
   !print*, "new version of gauleg"
   call gauleg(-1.d0, 1.d0, t_nk, w_nk, nfs_)
   return

   !print*, "old version of gauleg"
   select case (nfs_)
     case (2)
        t_nk(1)=-0.57735026919d0 ; w_nk(1)=1.d0
        t_nk(2)= 0.57735026919d0 ; w_nk(2)=1.d0
     case (3)
        t_nk(1)=-0.774596669241d0; w_nk(1)=0.555555555556d0
        t_nk(2)= 0.d0            ; w_nk(2)=0.888888888889d0
        t_nk(3)= 0.774596669241d0; w_nk(3)=0.555555555556d0
     case (4)
        t_nk(1)=-0.861136311594d0; w_nk(1)=0.347854845137d0
        t_nk(2)=-0.339981043585d0; w_nk(2)=0.652145154863d0
        t_nk(3)= 0.339981043585d0; w_nk(3)=0.652145154863d0
        t_nk(4)= 0.861136311594d0; w_nk(4)=0.347854845137d0
     case (5)
        t_nk(1)=-0.906179845939d0; w_nk(1)=0.236926885056d0
        t_nk(2)=-0.538469310106d0; w_nk(2)=0.478628670499d0
        t_nk(3)= 0.d0            ; w_nk(3)=0.568888888889d0
        t_nk(4)= 0.538469310106d0; w_nk(4)=0.478628670499d0
        t_nk(5)= 0.906179845939d0; w_nk(5)=0.236926885056d0
     case (6)
        t_nk(1)=-0.932469514203d0; w_nk(1)=0.171324492379d0
        t_nk(2)=-0.661209386466d0; w_nk(2)=0.360761573048d0
        t_nk(3)=-0.238619186083d0; w_nk(3)=0.467913934573d0
        t_nk(4)= 0.238619186083d0; w_nk(4)=0.467913934573d0
        t_nk(5)= 0.661209386466d0; w_nk(5)=0.360761573048d0
        t_nk(6)= 0.932469514203d0; w_nk(6)=0.171324492379d0
     case (7)
        t_nk(1)=-0.949107912343d0; w_nk(1)=0.129484966169d0
        t_nk(2)=-0.741531185599d0; w_nk(2)=0.279705391489d0
        t_nk(3)=-0.405845151377d0; w_nk(3)=0.381830050505d0
        t_nk(4)= 0.d0            ; w_nk(4)=0.417959183673d0
        do i = 1, 3
           t_nk(i+4) = -t_nk(4-i); w_nk(i+4) = w_nk(4-i)
        enddo
     case (8)
        t_nk(1)=-0.960289856498d0; w_nk(1)=0.10122853629d0
        t_nk(2)=-0.796666477414d0; w_nk(2)=0.222381034453d0
        t_nk(3)=-0.525532409916d0; w_nk(3)=0.313706645878d0
        t_nk(4)=-0.183434642496d0; w_nk(4)=0.362683783378d0
        do i = 1, 4
           t_nk(i+4) = -t_nk(5-i); w_nk(i+4) = w_nk(5-i)
        enddo
     case (9)
        t_nk(1)=-0.968160239508d0; w_nk(1)=0.0812743883616d0
        t_nk(2)=-0.836031107327d0; w_nk(2)=0.180648160695d0
        t_nk(3)=-0.613371432701d0; w_nk(3)=0.260610696403d0
        t_nk(4)=-0.324253423404d0; w_nk(4)=0.31234707704d0
        t_nk(5)= 0.d0            ; w_nk(5)=0.330239355001d0
        do i = 1, 4
           t_nk(i+5) = -t_nk(5-i); w_nk(i+5) = w_nk(5-i)
        enddo
     case (10)
        t_nk(1)=-0.973906528517d0; w_nk(1)=0.0666713443087d0
        t_nk(2)=-0.865063366689d0; w_nk(2)=0.149451349151d0
        t_nk(3)=-0.679409568299d0; w_nk(3)=0.219086362516d0
        t_nk(4)=-0.433395394129d0; w_nk(4)=0.26926671931d0
        t_nk(5)=-0.148874338982d0; w_nk(5)=0.295524224715d0
        do i = 1, 5
           t_nk(i+5) = -t_nk(6-i); w_nk(i+5) = w_nk(6-i)
        enddo
     case (11)
        t_nk(1)=-0.978228658146d0; w_nk(1)=0.0556685671162d0
        t_nk(2)=-0.887062599768d0; w_nk(2)=0.125580369465d0
        t_nk(3)=-0.730152005574d0; w_nk(3)=0.186290210928d0
        t_nk(4)=-0.519096129207d0; w_nk(4)=0.233193764592d0
        t_nk(5)=-0.269543155952d0; w_nk(5)=0.26280454451d0
        t_nk(6)= 0.d0            ; w_nk(6)=0.272925086778d0
        do i = 1, 5
           t_nk(i+6) = -t_nk(6-i); w_nk(i+6) = w_nk(6-i)
        enddo
     case (12)
        t_nk(1)=-0.981560634247d0; w_nk(1)=0.047175336386d0
        t_nk(2)=-0.904117256370d0; w_nk(2)=0.106939325995d0
        t_nk(3)=-0.769902674194d0; w_nk(3)=0.160078328543d0
        t_nk(4)=-0.587317954287d0; w_nk(4)=0.203167426723d0
        t_nk(5)=-0.367831498998d0; w_nk(5)=0.233492536538d0
        t_nk(6)=-0.125233408511d0; w_nk(6)=0.249147045813d0
        do i = 1, 6
           t_nk(i+6) = -t_nk(7-i); w_nk(i+6) = w_nk(7-i)
        enddo
     case default
        write(*,*) "gl_weight: illegal value of nfs_"
        stop
   end select
   !
   return
   !
end subroutine gl_weight
!----------------------------------------------------------------------
subroutine gauleg(x1,x2,x,w,n)
   !
   ! This routine compute abscissas and weights used in
   ! Gauss-Legendre integration 
   ! Adapted from gauleg.f routine in Numerical Recipe p.145
   !
   implicit none
   !
   ! I/O variables
   !
   integer, parameter :: dp=kind(0.D0)
   integer :: n           ! # of points used to approx. integral
   real(DP) :: x1, x2     ! lower and upper limits of the integral
                          ! ( must be -1 and 1 for both integration
                          !   over freq. and coupling const. )
   real(DP) :: x(n), w(n) ! arrays contain abscissas and weights
   !
   ! Local variables
   !
   real(DP), parameter :: eps=3.d-14
   integer :: i,j,m
   real(DP) :: p1,p2,p3,pp,xl,xm,z,z1
   !
   !
   m=(n+1)/2
   xm=0.5d0*(x2+x1)
   xl=0.5d0*(x2-x1)
   do 12 i=1,m
      z=cos(3.141592654d0*(i-.25d0)/(n+.5d0))
1     continue
         p1=1.d0
         p2=0.d0
         do 11 j=1,n
            p3=p2
            p2=p1
            p1=((2.d0*j-1.d0)*z*p2-(j-1.d0)*p3)/j
11       continue
         pp=n*(z*p1-p2)/(z*z-1.d0)
         z1=z
         z=z1-p1/pp
      if(abs(z-z1).gt.eps) goto 1
      x(i)=xm-xl*z
      x(n+1-i)=xm+xl*z
      w(i)=2.d0*xl/((1.d0-z*z)*pp*pp)
      w(n+1-i)=w(i)
12 continue
   !
   return
   !
end subroutine gauleg
