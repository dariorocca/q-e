!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------
!
! Lanczos algorithm solver 
! lc_lanczos_solver : Sums over the bands for rpa_only
! lc_lanczos_solver_v : Does not sum over the bands for beyond-RPA
!
SUBROUTINE lc_lanczos_solver(niterlan,pol)

    USE io_global,                ONLY : ionode, stdout
    USE kinds,                    ONLY : dp
    USE lc_variables,             ONLY : evc0, d0psi, naux, iun_alpha_beta, iun_zeta
    USE wvfct,                    ONLY : nbnd, npwx, npw, et
    USE control_flags,            ONLY : gamma_only
    USE gvect,                    ONLY : ngm
    USE klist,                    ONLY : ngk
    !
    IMPLICIT NONE
    !
    integer, intent(in) :: niterlan, pol
    !
    ! Local variables
    !
    complex(kind=dp), allocatable, dimension(:,:,:) :: evc1, evc1_new, evc1_old
    !
    integer :: ibnd, jbnd, ik, ig, i, ip
    !
    real(kind=dp) :: beta_s, alpha_s, zeta_s
    real(kind=dp) :: norm0_s
    !
    CALL start_clock('lc_lanczos_solver')
    !
    !
    allocate(evc1(npwx,nbnd,1))
    allocate(evc1_new(npwx,nbnd,1))
    allocate(evc1_old(npwx,nbnd,1))
    evc1=(0.d0,0.d0)
    evc1_new=(0.d0,0.d0)
    evc1_old=(0.d0,0.d0)
    !
    beta_s=0.d0
    alpha_s=0.d0
    zeta_s=0.d0
    !
    evc1(:,:,:)=d0psi(:,:,:,pol)
    !
    CALL lc_normalise( evc1(:,:,:), norm0_s )
    if (ionode) write(iun_alpha_beta,'(5X,"Norm of initial Lanczos vectors=",1x,d24.16)') norm0_s
    !
    !
    !
    do i=1,niterlan
       !
       do ip=1,naux
          !
          call lc_dot(d0psi(:,:,:,ip),evc1(:,:,:),zeta_s)
          if (ionode) write(iun_zeta,'(d24.16)') zeta_s
          !
       end do
       !
       CALL h_psi(npwx,ngk(1),nbnd,evc1(1,1,1),evc1_new(1,1,1))
       !
       do ibnd=1,nbnd
          !
          do ig=1,ngk(1)
             !
             evc1_new(ig,ibnd,1)=evc1_new(ig,ibnd,1)-cmplx(et(ibnd,1),0.0d0,dp)*evc1(ig,ibnd,1)
             !
          END DO
          !
       end do
       !
       CALL orthogonalize(evc1_new(:,:,1), evc0(:,:,1), &
             & 1, 1, evc0(:,:,1), ngk(1), .true.)
       evc1_new(:,:,1)=-evc1_new(:,:,1)
       !
       !
       if (i.ne.1) then
          evc1_new(:,:,1)=evc1_new(:,:,1)-beta_s*evc1_old(:,:,1)
       end if
       !
       call lc_dot(evc1(:,:,:),evc1_new(:,:,:),alpha_s)
       !
       if (ionode) write(iun_alpha_beta,'(5X,"alpha (",i8.8,")=",d24.16,2X,d24.16)') i,alpha_s
       evc1_new(:,:,1)=evc1_new(:,:,1)-alpha_s*evc1(:,:,1)
       !
       call lc_dot(evc1_new(1,1,1),evc1_new(1,1,1),beta_s)
          !
       beta_s=sqrt(beta_s)
       if (ionode) write(iun_alpha_beta,'(5X,"beta (",i8.8,")=",d24.16)') i, beta_s
       !
       !
       evc1_old(:,:,:)=evc1(:,:,:)
       evc1(:,:,1)=evc1_new(:,:,1)*cmplx(1.d0/beta_s,0.d0,dp)
       evc1_new(:,:,:)=(0.d0,0.d0)
       !
    end do
    !
    !
    deallocate(evc1)
    deallocate(evc1_new)
    deallocate(evc1_old)
    !
    CALL stop_clock ('lc_lanczos_solver')
    !
    return
    !
END SUBROUTINE lc_lanczos_solver
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
SUBROUTINE lc_lanczos_solver_v(niterlan,pol)
    
    USE io_global,                ONLY : ionode, stdout
    USE kinds,                    ONLY : dp
    USE lc_variables,             ONLY : norm0, evc0, d0psi, naux
    USE wvfct,                    ONLY : nbnd, npwx, npw, et
    USE control_flags,            ONLY : gamma_only
    USE gvect,                    ONLY : ngm
    USE klist,                    ONLY : ngk
    !
    IMPLICIT NONE
    !
    integer, intent(in) :: niterlan, pol
    !
    ! Local variables
    !
    complex(kind=dp), allocatable, dimension(:,:,:) :: evc1, evc1_new, evc1_old
    !
    integer :: ibnd, jbnd, ik, ig, i, ip
    !
    real(kind=dp), allocatable :: beta(:), alpha(:), zeta(:,:)
    !
    CALL start_clock('lc_lanczos_solver_v')
    !
    !
    allocate(evc1(npwx,nbnd,1))
    allocate(evc1_new(npwx,nbnd,1))
    allocate(evc1_old(npwx,nbnd,1))
    evc1=(0.d0,0.d0)
    evc1_new=(0.d0,0.d0)
    evc1_old=(0.d0,0.d0)
    !
    allocate(beta(nbnd))
    allocate(alpha(nbnd))
    allocate(zeta(nbnd,nbnd))
    beta=0.d0
    alpha=0.d0
    zeta=0.d0
    !
    evc1(:,:,:)=d0psi(:,:,:,pol)
    !
    CALL lc_normalise_v( evc1(:,:,:), norm0(:,pol) )
    do ibnd=1,nbnd
       write(stdout,'(5X,"Norm (",2i4.4,")=",d24.16)') ibnd, pol, norm0(ibnd,pol)
    end do
    !
    !
    if (ionode) open (158, file = "zeta_store", form = 'formatted', position='append', status = 'unknown')
    !
    !
    do i=1,niterlan
       !
       do ip=1,naux
          !
          call lc_dot_m(d0psi(:,:,:,ip),evc1(:,:,:),zeta)
          do ibnd=1,nbnd
             do jbnd=1,nbnd
                if (ionode) write(158,'(d24.16)') zeta(ibnd,jbnd)
             end do
          end do
          !
       end do
       !
       !
       CALL h_psi(npwx,ngk(1),nbnd,evc1(1,1,1),evc1_new(1,1,1))
       !
       do ibnd=1,nbnd
          !
          do ig=1,ngk(1)
             !
             evc1_new(ig,ibnd,1)=evc1_new(ig,ibnd,1)-cmplx(et(ibnd,1),0.0d0,dp)*evc1(ig,ibnd,1)
             !
          END DO
          !
       end do
       !
       CALL orthogonalize(evc1_new(:,:,1), evc0(:,:,1), &
             & 1, 1, evc0(:,:,1), ngk(1), .true.)
       evc1_new(:,:,1)=-evc1_new(:,:,1)
       !
       !
       if (i.ne.1) then
          do ibnd=1,nbnd
             evc1_new(:,ibnd,1)=evc1_new(:,ibnd,1)-beta(ibnd)*evc1_old(:,ibnd,1)
          end do
       end if
       !
       call lc_dot_v(evc1(:,:,:),evc1_new(:,:,:),alpha)
       !
       do ibnd=1,nbnd
          write(stdout,'(5X,"alpha (",2i8.8,")=",d24.16,2X,d24.16)') i,ibnd,alpha(ibnd)
          evc1_new(:,ibnd,1)=evc1_new(:,ibnd,1)-alpha(ibnd)*evc1(:,ibnd,1)
       end do
       !
       call lc_dot_v(evc1_new(1,1,1),evc1_new(1,1,1),beta)
       !
       beta=sqrt(beta)
       do ibnd=1,nbnd
          write(stdout,'(5X,"beta (",2i8.8,")=",d24.16)') i,ibnd,beta(ibnd)
       end do
       !
       evc1_old(:,:,:)=evc1(:,:,:)
       do ibnd=1,nbnd
          evc1(:,ibnd,1)=evc1_new(:,ibnd,1)*cmplx(1.d0/beta(ibnd),0.d0,dp)
       end do
       evc1_new(:,:,:)=(0.d0,0.d0)
       !
    end do
    !
    if (ionode) close(158)
    !
    deallocate(evc1)
    deallocate(evc1_new)
    deallocate(evc1_old)
    deallocate(beta, alpha, zeta)
    !
    CALL stop_clock ('lc_lanczos_solver_v')
    !
    return
    !
END SUBROUTINE lc_lanczos_solver_v 
