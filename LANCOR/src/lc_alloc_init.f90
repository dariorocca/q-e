!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE lc_alloc_init()
  !---------------------------------------------------------------------
  !
  ! This subroutine allocates and initialises some variables used by the LANCOR
  ! code.
  !
  USE lc_variables
  USE kinds,                ONLY : dp
  USE ions_base,            ONLY : nat
  USE uspp,                 ONLY : nkb, okvan
  USE uspp_param,           ONLY : nhm
  USE fft_base,             ONLY : dfftp, dffts
  USE klist,                ONLY : nks
  USE lsda_mod,             ONLY : nspin
  USE wvfct,                ONLY : npwx, nbnd
  USE control_flags,        ONLY : gamma_only
  USE io_global,            ONLY : stdout
  USE noncollin_module,     ONLY : nspin_mag, npol, noncolin
  USE eqv,                  ONLY : dmuxc, evq, dpsi, dvpsi
  USE wavefunctions,        ONLY : evc
  USE qpoint,               ONLY : igkq, nksq, eigqts
  USE becmod,               ONLY : allocate_bec_type, bec_type, becp
  USE control_lr,           ONLY : nbnd_occ
  !
  IMPLICIT NONE
  !
  INTEGER :: ik
  !
  IF (dffts%has_task_groups) THEN
     CALL errore( 'lc_alloc_init', 'task groups not supported', 1 )
  END IF
  !
  IF (allocated(evc)) THEN
     DEALLOCATE(evc)
     ALLOCATE(evc(npwx,nbnd))
  ENDIF
  !
  ! Allocate unperturbed orbitals
  !
  ALLOCATE(evc0(npwx,nbnd,1))
  !
  !
  evc0(:,:,:)       = (0.0d0,0.0d0)
  !
  ! Allocate the R-space unperturbed orbitals
  !
  ALLOCATE(revc0(dffts%nnr,nbnd,1))
  revc0(:,:,:) = 0.0d0
  !
  !
  allocate(norm0(nbnd,naux))
  !
  CALL lc_alloc_init_gamma()
  !
  RETURN
  !
CONTAINS
  !
  SUBROUTINE lc_alloc_init_gamma()
    !
    IF (nkb > 0) THEN
       !
       IF (.not. allocated(becp%r)) CALL allocate_bec_type(nkb,nbnd,becp)
       becp%r(:,:) = 0.0d0
       !
    ENDIF
    !
    RETURN
    !
  END SUBROUTINE lc_alloc_init_gamma
  !
END SUBROUTINE lc_alloc_init
!----------------------------------------------------------------------------
