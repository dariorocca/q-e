!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------------
SUBROUTINE lc_print_clock()
   !---------------------------------------------------------------------------
   !
   ! This subroutine prints out the clocks at the end of the run.
   !
   USE io_global,        ONLY : stdout
   USE mp_world,         ONLY : mpime, root
   USE lc_variables,     ONLY : rpa_only
   !
   IMPLICIT NONE
   !
   IF ( mpime /= root ) &
      OPEN( UNIT = stdout, FILE = '/dev/null', STATUS = 'UNKNOWN' )
   !
   WRITE( stdout, * )
   !
   CALL print_clock( 'lc_main' )
   !
   CALL print_clock( 'lc_read_wf' )
   !
!   IF (lanczos) then
      IF (rpa_only) THEN
         CALL print_clock( 'lc_lanczos_solver' )
      ELSE
         CALL print_clock( 'lc_lanczos_solver_v' )
      END IF
!   ELSE
      CALL print_clock( 'lc_cg_solver' )
!   END IF
   !
   WRITE( stdout, * )
   !
   CALL print_clock( 'h_psi' )
!   CALL print_clock( 'lc_ortho' )
   IF (rpa_only) THEN
      CALL print_clock( 'lc_dot' )
   ELSE
      CALL print_clock( 'lc_dot_v' )
      CALL print_clock( 'lc_dot_m' )
   END IF
   !
   WRITE( stdout, * ) 
   !
   WRITE( stdout, '(5X,"General routines")' )
   !
   CALL print_clock( 'calbec' )
   CALL print_clock( 'fft' )
   CALL print_clock( 'ffts' )
   CALL print_clock( 'fftc' )
   CALL print_clock( 'fftw' )
   CALL print_clock( 'fftcw' )
   CALL print_clock( 'interpolate' )
   CALL print_clock( 'davcio' )
   CALL print_clock( 'newq' )
   !
   WRITE( stdout, * )
   !
#if defined (__MPI)
   WRITE( stdout, '(5X,"Parallel routines")' )
   CALL print_clock( 'fft_scatter' )
   CALL print_clock ('mp_sum')
   WRITE( stdout, * )
#endif
   !
   RETURN
   !
END SUBROUTINE lc_print_clock
