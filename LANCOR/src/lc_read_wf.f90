!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE lc_read_wf()
  !---------------------------------------------------------------------
  !
  ! Reads in and stores the ground state wavefunctions
  ! for use in ACFDT calculations
  !
  USE kinds,                ONLY : dp
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nks, xk
  USE cell_base,            ONLY : tpiba2
  USE gvect,                ONLY : ngm, g
  USE io_files,             ONLY : nwordwfc, iunwfc, prefix, diropn,&
                                 & tmp_dir, wfc_dir 
  USE lc_variables,         ONLY : evc0, revc0
  USE wvfct,                ONLY : npw, nbnd, g2kin, npwx
  USE control_flags,        ONLY : gamma_only,io_level
  USE fft_base,             ONLY : dffts
  USE fft_interfaces,       ONLY : invfft
  USE uspp,                 ONLY : vkb, nkb, okvan
  USE becmod,               ONLY : bec_type, becp, calbec
  USE realus,               ONLY : real_space, invfft_orbital_gamma,&
                                 & initialisation_level,&
                                 & fwfft_orbital_gamma, calbec_rs_gamma
  USE buffers,              ONLY : get_buffer
  USE wavefunctions,        ONLY : evc
  USE buffers,              ONLY : open_buffer
  USE qpoint,               ONLY : nksq
  USE noncollin_module,     ONLY : npol
  !
  IMPLICIT NONE
  !
  ! local variables
  !
  INTEGER :: ik, ibnd, ig, itmp1,itmp2,itmp3
  LOGICAL :: exst
  CHARACTER(len=256) :: filename, tmp_dir_saved
  !
  CALL start_clock('lc_read_wf')
  !
  CALL normal_read()
  !
  !WRITE(stdout,'(5x,"Finished reading wfc.")')
  !
  evc(:,:) = evc0(:,:,1)
  !
  !
  CALL stop_clock('lc_read_wf')
  !
  RETURN
  !
  CONTAINS
    
SUBROUTINE normal_read()
  !
  USE wavefunctions,            ONLY : psic
  USE realus,                   ONLY : tg_psic
  USE mp_global,                ONLY : me_bgrp
  !
  IMPLICIT NONE
  !
  LOGICAL :: use_tg
  INTEGER :: v_siz, incr, ioff, j
  !
  WRITE( stdout, '(/5x,"Normal read")' )
  !
  use_tg = dffts%has_task_groups
  incr = 2
  !
  ! Read in the ground state wavefunctions.
  ! This is a parallel read, done in wfc_dir.
  !
  tmp_dir_saved = tmp_dir
  !   
  wfc_dir = tmp_dir
  !
  CALL diropn ( iunwfc, 'wfc', 2*nwordwfc, exst)
  !
  IF (.NOT.exst .AND. wfc_dir == 'undefined') &
      & CALL errore('lc_read_wfc', TRIM( prefix )//'.wfc'//' not found',1) 
  !
  IF (.NOT.exst .AND. wfc_dir /= 'undefined') THEN
     !
     WRITE( stdout, '(/5x,"Attempting to read wfc from outdir instead of wfcdir")' ) 
     CLOSE( UNIT = iunwfc)
     tmp_dir = tmp_dir_saved
     CALL diropn ( iunwfc, 'wfc', 2*nwordwfc, exst)
     IF (.NOT.exst) CALL errore('lc_read_wfc', TRIM( prefix )//'.wfc'//' not found',1)
     !
  ENDIF
  !
  IF (gamma_only) THEN
     WRITE( stdout, '(/5x,"Gamma point algorithm")' )
  ELSE
     CALL errore('lc_read_wfc', 'k-points not implemented',1)
  ENDIF
  !
  !
  ik=1
  CALL davcio(evc0(:,:,ik),2*nwordwfc,iunwfc,ik,-1)
  !
  !
  CLOSE( UNIT = iunwfc)
  ! 
  ! End of file reading
  !
  tmp_dir = tmp_dir_saved
  !
  ! Calculation of the unperturbed wavefunctions in R-space revc0.
  ! Inverse Fourier transform of evc0.
  !
  IF ( dffts%has_task_groups ) THEN
       !
!       v_siz =  dffts%tg_nnr * dffts%nogrp
!       incr = 2 * dffts%nogrp
!       tg_revc0 = (0.0d0,0.0d0)
       !
  ELSE
       !
       revc0 = 0.0d0
       !
  ENDIF
  !
  IF ( gamma_only ) THEN
     !
     DO ibnd = 1, nbnd, 2
        !
        CALL invfft_orbital_gamma ( evc0(:,:,1), ibnd, nbnd)
        !
        IF (dffts%has_task_groups) THEN               
           !
!           DO j = 1, dffts%nr1x*dffts%nr2x*dffts%tg_npp( me_bgrp + 1 )
               !
!               tg_revc0(j,ibnd,1) = tg_psic(j)
               !  
!           ENDDO
           !
        ELSE
           !
           revc0(1:dffts%nnr,ibnd,1) = real(psic(1:dffts%nnr))
           if (ibnd<nbnd) revc0(1:dffts%nnr,ibnd+1,1) = aimag(psic(1:dffts%nnr))
           !
        ENDIF
        !
     ENDDO
     !
  ELSE
  !
  ENDIF
  !
!  IF ( real_space_debug > 0 .AND. .NOT. gamma_only ) &
!           CALL errore( ' iosys ', ' Linear response calculation ' // &
!           & 'real space algorithms with k-points not implemented', 1 )
  !
  RETURN
  !
END SUBROUTINE normal_read
    
END SUBROUTINE lc_read_wf
