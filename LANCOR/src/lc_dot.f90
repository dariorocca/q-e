!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
SUBROUTINE lc_dot(x,y,res_prod)
  !---------------------------------------------------------------------
  !
  ! This subroutine calculates a dot product of the conjugate 
  ! of a complex vector x and a complex vector y 
  ! (sum over the bands).
  !
  USE kinds,                ONLY : dp
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nks, xk, wk, ngk
  USE lsda_mod,             ONLY : nspin
  USE wvfct,                ONLY : npwx,nbnd,wg,npw,g2kin
  USE control_flags,        ONLY : gamma_only
  USE gvect,                ONLY : gstart, ngm, g
  USE cell_base,            ONLY : tpiba2
  USE mp,                   ONLY : mp_sum
  USE mp_global,            ONLY : inter_pool_comm, intra_bgrp_comm
  USE qpoint,               ONLY : npwq, igkq, ikks, ikqs, nksq
  !
  IMPLICIT NONE
  !
  COMPLEX(kind=dp) :: x(npwx,nbnd,1), y(npwx,nbnd,1)
  REAL(kind=dp), INTENT(out) :: res_prod
  REAL(kind=dp) :: degspin
  INTEGER :: ibnd
  REAL(kind=dp), EXTERNAL    :: DDOT
  !
  CALL start_clock ('lc_dot')
  !
  res_prod = 0.0d0
  !
  IF (nspin==2) THEN
      degspin = 1.0d0
  ELSE
      degspin = 2.0d0
  ENDIF
  !
  IF (gamma_only) THEN
     !
     DO ibnd=1,nbnd
        !
        res_prod = res_prod + 2.D0*wg(ibnd,1)*DDOT(2*ngk(1),x(:,ibnd,1),1,y(:,ibnd,1),1)
        !
        ! G=0 has been accounted twice, so we subtract one contribution.
        !
        IF (gstart==2) res_prod = res_prod - wg(ibnd,1)*dble(x(1,ibnd,1))*dble(y(1,ibnd,1))
        !
     ENDDO
     !
#ifdef __MPI
     CALL mp_sum(res_prod, intra_bgrp_comm)
#endif
     !
  ENDIF
  !
  res_prod = res_prod/degspin
  !
  CALL stop_clock ('lc_dot')
  !
END SUBROUTINE lc_dot
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
SUBROUTINE lc_dot_v(x,y,res_prod)
  !---------------------------------------------------------------------
  !
  ! This subroutine calculates a dot product of the conjugate 
  ! of a complex vector x and a complex vector y. 
  ! NO sum over the bands: It returns a nbnd vector
  !
  USE kinds,                ONLY : dp
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nks, xk, wk, ngk
  USE lsda_mod,             ONLY : nspin
  USE wvfct,                ONLY : npwx,nbnd,wg,npw,g2kin
  USE control_flags,        ONLY : gamma_only
  USE gvect,                ONLY : gstart, ngm, g
  USE cell_base,            ONLY : tpiba2
  USE mp,                   ONLY : mp_sum
  USE mp_global,            ONLY : inter_pool_comm, intra_bgrp_comm
  USE qpoint,               ONLY : npwq, igkq, ikks, ikqs, nksq
  !
  IMPLICIT NONE
  !
  COMPLEX(kind=dp) :: x(npwx,nbnd,1), y(npwx,nbnd,1)
  REAL(kind=dp), INTENT(out) :: res_prod(nbnd)
  REAL(kind=dp) :: degspin
  INTEGER :: ibnd
  REAL(kind=dp), EXTERNAL    :: DDOT
  !
  CALL start_clock ('lc_dot_v')
  !
  res_prod = 0.0d0
  !
  IF (nspin==2) THEN
      degspin = 1.0d0
  ELSE
      degspin = 2.0d0
  ENDIF
  !
  IF (gamma_only) THEN
     !
     DO ibnd=1,nbnd
        !
        res_prod(ibnd) = res_prod(ibnd) + 2.D0*wg(ibnd,1)*DDOT(2*ngk(1),x(:,ibnd,1),1,y(:,ibnd,1),1)
        !
        ! G=0 has been accounted twice, so we subtract one contribution.
        !
        IF (gstart==2) res_prod(ibnd) = res_prod(ibnd) - wg(ibnd,1)*dble(x(1,ibnd,1))*dble(y(1,ibnd,1))
        !
     ENDDO
     !
#ifdef __MPI
     CALL mp_sum(res_prod, intra_bgrp_comm)
#endif
     !
  ENDIF
  !
  res_prod = res_prod/degspin
  !
  CALL stop_clock ('lc_dot_v')
  !
END SUBROUTINE lc_dot_v
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
SUBROUTINE lc_dot_m(x,y,z_prod)
  !---------------------------------------------------------------------
  !
  ! This subroutine calculates a dot product of the conjugate 
  ! of a complex vector x and a complex vector y. 
  ! NO sums over bands: It returns a nbnd*nbnd matrix.
  ! Differently from lc_dot_v it considers the band indexes
  ! ibnd and jbnd as different.
  !
  USE kinds,                ONLY : dp
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nks, xk, wk, ngk
  USE lsda_mod,             ONLY : nspin
  USE wvfct,                ONLY : npwx,nbnd,wg,npw,g2kin
  USE control_flags,        ONLY : gamma_only
  USE gvect,                ONLY : gstart, ngm, g
  USE cell_base,            ONLY : tpiba2
  USE mp,                   ONLY : mp_sum
  USE mp_global,            ONLY : inter_pool_comm, intra_bgrp_comm
  USE qpoint,               ONLY : npwq, igkq, ikks, ikqs, nksq
  !
  IMPLICIT NONE
  !
  COMPLEX(kind=dp) :: x(npwx,nbnd,1), y(npwx,nbnd,1)
  REAL(kind=dp), INTENT(out) :: z_prod(nbnd,nbnd)
  REAL(kind=dp) :: degspin
  INTEGER :: ibnd, jbnd
  REAL(kind=dp), EXTERNAL    :: DDOT
  !
  CALL start_clock ('lc_dot_m')
  !
  z_prod = 0.0d0
  !
  IF (nspin==2) THEN
      degspin = 1.0d0
  ELSE
      degspin = 2.0d0
  ENDIF
  !
  IF (gamma_only) THEN
     !
     DO ibnd=1,nbnd
        !
        DO jbnd=1,nbnd
           !
           z_prod(ibnd,jbnd) = z_prod(ibnd,jbnd) + 2.D0*wg(ibnd,1)*DDOT(2*ngk(1),x(:,ibnd,1),1,y(:,jbnd,1),1)
           !
           ! G=0 has been accounted twice, so we subtract one contribution.
           !
           IF (gstart==2) z_prod(ibnd,jbnd) = z_prod(ibnd,jbnd) - wg(ibnd,1)*dble(x(1,ibnd,1))*dble(y(1,jbnd,1))
           !
        ENDDO
        !
     ENDDO
     !
#ifdef __MPI
     CALL mp_sum(z_prod, intra_bgrp_comm)
#endif
     !
  ENDIF
  !
  z_prod = z_prod/degspin
  !
  CALL stop_clock ('lc_dot_m')
  !
END SUBROUTINE lc_dot_m
!-----------------------------------------------------------------------
