!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!----------------------------------------------------------------------
!
! Normalise a vector
! lc_normalise : Sums over the bands for rpa_only
! lc_normalise_v : Does not sum over the bands for beyond-RPA
!
SUBROUTINE lc_normalise (evc1, norm)
  !--------------------------------------------------------------------
  !
  ! This subroutine normalises the two components of a super-vector 
  ! so that they have an inner product equal to 1.
  !
  USE kinds,                ONLY : dp
  USE gvect,                ONLY : gstart
  USE cell_base,            ONLY : omega
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nks,xk
  USE lsda_mod,             ONLY : nspin
  USE uspp,                 ONLY : vkb, nkb, okvan
  USE wvfct,                ONLY : nbnd, npwx, npw, wg
  USE control_flags,        ONLY : gamma_only
  USE noncollin_module,     ONLY : npol
  USE qpoint,               ONLY : nksq
  !
  IMPLICIT NONE
  !
  COMPLEX(kind=dp), INTENT(inout) :: evc1(npwx,nbnd,1)
  REAL(kind=dp), INTENT(out) :: norm
  !
  ! local variables
  !
  INTEGER :: ik, ibnd
  !
  real(kind=dp) :: prod
  !
  if (gamma_only) then
    !
    !
    call lc_dot(evc1(1,1,1), evc1(1,1,1), prod)
    prod=1.0d0/sqrt(prod)
    !
    evc1(:,:,1)=cmplx(prod,0.0d0,dp)*evc1(:,:,1)
    !
    norm=1.0d0/prod
    !
  endif
  !
END SUBROUTINE lc_normalise
!-----------------------------------------------------------------------
!----------------------------------------------------------------------
SUBROUTINE lc_normalise_v (evc1, norm)
  !--------------------------------------------------------------------
  !
  ! This subroutine normalises the two components of a super-vector 
  ! so that they have an inner product equal to 1.
  !
  USE kinds,                ONLY : dp
  USE gvect,                ONLY : gstart
  USE cell_base,            ONLY : omega
  USE io_global,            ONLY : stdout
  USE klist,                ONLY : nks,xk
  USE lsda_mod,             ONLY : nspin
  USE uspp,                 ONLY : vkb, nkb, okvan
  USE wvfct,                ONLY : nbnd, npwx, npw, wg
  USE control_flags,        ONLY : gamma_only
  USE noncollin_module,     ONLY : npol
  USE qpoint,               ONLY : nksq
  !
  IMPLICIT NONE
  !
  COMPLEX(kind=dp), INTENT(inout) :: evc1(npwx,nbnd,1)
  REAL(kind=dp), INTENT(out) :: norm(nbnd)
  !
  ! local variables
  !
  INTEGER :: ik, ibnd
  !
  real(kind=dp) :: prod(nbnd)
  !
  if (gamma_only) then
    !
    !
    call lc_dot_v(evc1(1,1,1), evc1(1,1,1), prod)
    prod=1.0d0/sqrt(prod)
    !
    do ibnd=1,nbnd
       evc1(:,ibnd,1)=cmplx(prod(ibnd),0.0d0,dp)*evc1(:,ibnd,1)
    end do
    !
    do ibnd=1,nbnd
       write(stdout,'(5X,"Norm of initial Lanczos vectors=",1x,d24.16)') 1.0d0/prod(ibnd)
       norm(ibnd)=1.0d0/prod(ibnd)
    end do
  !
  endif
  !
END SUBROUTINE lc_normalise_v
!-----------------------------------------------------------------------
