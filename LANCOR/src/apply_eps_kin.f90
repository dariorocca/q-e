!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
SUBROUTINE apply_eps_kin( veh_tmp )
  !
  ! This subroutine applies a dielectric matrix that contains only
  ! the kinetic energy contribution to a vector
  ! See D. Rocca The Journal of Chemical Physics 140, 18A501 (2014)
  !
  use cell_base,            only : tpiba2
  USE fft_base,             ONLY : dffts
  use kinds,                only : dp
  use gvect,                only : g
  use lc_variables,         only : evc0, revc0
  use wvfct,                only : nbnd, g2kin, npwx, et
  USE wavefunctions,        ONLY : psic
  use gvect,                only : gstart
  USE constants,            ONLY : e2, fpi
  USE klist,                ONLY : igk_k, ngk
  USE control_flags,        ONLY : gamma_only
  USE realus,               ONLY : invfft_orbital_gamma, fwfft_orbital_gamma
  USE fft_interfaces,       ONLY : invfft, fwfft
  !
  COMPLEX(kind=dp) :: veh_tmp(npwx)
  REAL(DP) :: qg22, fac2
  COMPLEX(kind=dp), allocatable :: d0psi_tmp(:,:)
  REAL(kind=dp), allocatable :: veh_sav(:)
  INTEGER :: ig, ibnd
  REAL (DP), EXTERNAL :: DDOT
  INTEGER :: nrxx
  !
  nrxx = dffts%nnr
  !
  allocate(veh_sav(nrxx))
  allocate(d0psi_tmp(npwx,nbnd))
  veh_sav(:)=0.d0
  d0psi_tmp=(0.d0,0.d0)
  ! 
  DO ig = 1, ngk(1)
     !  
     qg22 = (g(1,igk_k(ig,1)))**2 + (g(2,igk_k(ig,1)))**2 + (g(3,igk_k(ig,1)))**2
     IF (qg22 > 1.d-8) then
        fac2 = (e2*fpi)/(qg22*tpiba2)
        veh_tmp(ig) = veh_tmp(ig) * sqrt(fac2)
     ELSE
        veh_tmp(ig) = (0.D0,0.D0)
     END IF
     !
  ENDDO
  !
  psic(:)=(0.d0,0.d0)
  !
  DO ig = 1, ngk(1)
     !
     psic(dffts%nl(igk_k(ig,1)))=veh_tmp(ig)
     psic(dffts%nlm(igk_k(ig,1)))=conjg(veh_tmp(ig))
     !
  ENDDO
  !
  CALL invfft ('Wave', psic, dffts)
  !
  veh_sav(:)=dble(psic(:))
  !
  DO ibnd=1,nbnd,2                     !!!!!!!!!!!!!!!!!!!!!! BANDS !!!!!!!!!!!!!!!!!!!!!
     !
     psic(:)=(0.D0,0.D0)
     if (ibnd<nbnd) then 
        psic(:)=cmplx(veh_sav(:)*revc0(:,ibnd,1),veh_sav(:)*revc0(:,ibnd+1,1),dp)
     else 
        psic(:)=cmplx(veh_sav(:)*revc0(:,ibnd,1),0.d0,dp)
     end if
     !
     CALL fwfft_orbital_gamma(d0psi_tmp,ibnd,nbnd)
     !  
  ENDDO
  !
  CALL orthogonalize(d0psi_tmp(:,:), evc0(:,:,1), &
                   & 1, 1, evc0(:,:,1), ngk(1), .true.)
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! PRECONDITION HERE
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  DO ibnd=1,nbnd
     DO ig = 1, ngk(1)
        !
        d0psi_tmp(ig,ibnd) = 1.d0/(g2kin(ig)-et(ibnd,1)) * d0psi_tmp(ig,ibnd)
        !
     ENDDO
  ENDDO
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! END PRECONDITIONING
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !      
  CALL orthogonalize(d0psi_tmp(:,:), evc0(:,:,1), &
                   & 1, 1, evc0(:,:,1), ngk(1), .true.)
  !
  veh_sav=0.d0
  !
  DO ibnd=1,nbnd,2   !!!!!!!!!!!!!!!!!!!!!! BANDS !!!!!!!!!!!!!!!!!!!!!
     !
     CALL invfft_orbital_gamma ( d0psi_tmp(:,:), ibnd, nbnd)
     !
     veh_sav(:)=veh_sav(:)+dble(psic(:))*revc0(:,ibnd,1)
     IF (ibnd<nbnd) veh_sav(:)=veh_sav(:)+aimag(psic(:))*revc0(:,ibnd+1,1)
     !
  ENDDO    !!!!!!!!!!!!!!!!!!!!!! BANDS !!!!!!!!!!!!!!!!!!!!!
  !
  psic=cmplx(veh_sav(:),0.d0,dp)
  CALL fwfft ('Wave', psic, dffts)
  !  
  DO ig = 1, ngk(1)
     veh_tmp(ig)=psic(dffts%nl(igk_k(ig,1)))
  ENDDO
  !
  DO ig = 1, ngk(1)
     !  
     qg22 = (g(1,ig))**2 + (g(2,ig))**2 + (g(3,ig))**2
     IF (qg22 > 1.d-8) fac2 = (e2*fpi)/(qg22*tpiba2)
     IF (qg22 > 1.d-8) veh_tmp (ig) = veh_tmp (ig) * sqrt(fac2)
     !  
  ENDDO
  !
  veh_tmp=-veh_tmp
  !
  DEALLOCATE(veh_sav,d0psi_tmp)
  !
  RETURN
  !
END SUBROUTINE apply_eps_kin
