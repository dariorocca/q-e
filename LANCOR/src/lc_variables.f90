!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!---------------------------------------------------------------------------
MODULE lc_variables
  !--------------------------------------------------------------------------
  ! Some global variables specific to the LANCOR code
  !--------------------------------------------------------------------------
  !
  USE kinds,                ONLY : dp
  USE control_flags,        ONLY : gamma_only
  !
  IMPLICIT NONE
  !
  ! Parameters
  !
  INTEGER :: nwordd0psi, nwordrestart, naux
  INTEGER :: naux_from_file
  !
  !------------------------------------------------------------------------!
  !
  COMPLEX(kind=dp), ALLOCATABLE :: &
       evc0(:,:,:),       &    ! the ground state wavefunctions (plane wave, band, k point)
       d0psi(:,:,:,:)          ! for saving the original starting vectors
  REAL(kind=dp), ALLOCATABLE :: revc0(:,:,:)            ! ground state wavefunctions in real space
  !
  ! lc_input
  !
  INTEGER :: niterlan            ! number of Lanczos vectors to be calculated
  INTEGER :: nfreq
  !
  LOGICAL :: rpa_only
  COMPLEX(kind=dp), allocatable :: veh(:,:)
  REAL(kind=dp), allocatable :: norm0(:,:) 
  REAL(kind=dp), allocatable :: eigenval_eps(:)

  INTEGER :: iun_alpha_beta=113, iun_zeta=114
  
  CHARACTER (len=10), PARAMETER :: code1 = 'LANCOR'

END MODULE lc_variables
