!
! Copyright (C) 2001-2015 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-------------------------------------------------------------
SUBROUTINE lc_init_nfo()
  !-------------------------------------------------------------
  !
  !  This subroutine prepares several variables which are needed in the
  !  LANCOR program
  !
  USE kinds,                ONLY : DP
  USE ions_base,            ONLY : nat, tau
  USE klist,                ONLY : nks,degauss,lgauss,ngauss,xk,wk,nelec, &
                                  & two_fermi_energies, nelup, neldw
  USE wvfct,                ONLY : nbnd, et, npw, g2kin
  USE klist,                ONLY : ngk, igk_k
  USE io_global,            ONLY : stdout
  USE constants,            ONLY : pi, tpi, degspin, eps8
  USE noncollin_module,     ONLY : noncolin, npol
!  USE mp,                   ONLY : mp_max, mp_min
  USE mp_global,            ONLY : inter_pool_comm
  USE gvect,                ONLY : ngm, g
  USE cell_base,            ONLY : at, bg, tpiba, tpiba2, omega
  USE ener,                 ONLY : ef, ef_up, ef_dw
  USE lsda_mod,             ONLY : lsda, current_spin, nspin, isk
  USE wvfct,                ONLY : npwx, wg
  USE gvecw,                ONLY : ecutwfc
  USE io_files,             ONLY : seqopn, tmp_dir, prefix, &
                                 & diropn, nwordwfc, wfc_dir
  USE qpoint,               ONLY : xq, npwq, igkq, ikks, ikqs, nksq, eigqts
  USE gvecs,                ONLY : doublegrid
  USE fft_base,             ONLY : dfftp
  USE uspp,                 ONLY : vkb, okvan, nkb
  USE wavefunctions,        ONLY : evc
  USE becmod,               ONLY : calbec, allocate_bec_type
  USE eqv,                  ONLY : evq
  USE control_flags,        ONLY : gamma_only
  USE control_lr,           ONLY : nbnd_occ
  !
  IMPLICIT NONE
  !
  ! local variables
  !
  REAL(kind=DP) :: small, emin, emax, xmax, fac, targ, arg
  INTEGER       :: i, ik, ibnd, ipol, ikk, ikq, ios, isym, na, ig
  REAL(DP), ALLOCATABLE :: wg_up(:,:), wg_dw(:,:)
  LOGICAL       :: exst ! logical variable to check file existence
  !
  ! 1) Initialize igk_k and npw_k
  !    Open shell related
  !
     !
     !IF ( .not. allocated( igk_k ) )    ALLOCATE(igk_k(npwx,nks))
     !IF ( .not. allocated( npw_k ) )    ALLOCATE(npw_k(nks))
     !
     !CALL seqopn( iunigk, 'igk', 'UNFORMATTED', exst )
     !
        !
        DO ik = 1, nks
           !
           CALL gk_sort( xk(1,ik), ngm, g, ( ecutwfc / tpiba2 ), ngk(ik), igk_k(:,ik), g2kin )
           !
           !IF ( nks > 1 ) WRITE( iunigk ) igk
           !
        ENDDO
        !
  if (gamma_only) then
     !
     do ig=1,npwx
        !
        g2kin(ig)=((xk(1,1)+g(1,igk_k(ig,1)))**2 &
                 +(xk(2,1)+g(2,igk_k(ig,1)))**2 &
                 +(xk(3,1)+g(3,igk_k(ig,1)))**2)*tpiba2
        !
     enddo
     !
     CALL init_us_2(ngk(1),igk_k(1,1),xk(1,1),vkb)
     !
  end if
  !
  !
  ! 2) Compute the number of occupied bands for each k point
  !
  CALL setup_nbnd_occ()
  !
  WRITE(stdout,*)
  WRITE(stdout,'("NPWX=",I15)') npwx
  WRITE(stdout,'("NBND=",I15)') nbnd
  WRITE(stdout,'("OCCUPIED NBND=",I15)') nbnd_occ(1)
  WRITE(stdout,'("NKS=",I15)') nks
  WRITE(stdout,'("NRXX=",I15)') dfftp%nnr
  !
  IF (nbnd>nbnd_occ(1)) THEN
     !
     nbnd = nbnd_occ(1)
     !
  ENDIF
  !
  ! The length of the arrays d0psi, evc1 etc.
  !
!  nwordd0psi   = 2 * nbnd * npwx
  nwordwfc     =     nbnd * npwx
  !
  !
  RETURN
  !
END SUBROUTINE lc_init_nfo
