!----------------------------------------------------------------------------
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!----------------------------------------------------------------------------
SUBROUTINE lc_cg_solver( )
  !----------------------------------------------------------------------------
  !
  ! ... "poor man" iterative diagonalization of a complex hermitian matrix
  ! ... through preconditioned conjugate gradient algorithm
  ! ... Band-by-band algorithm with minimal use of memory
  ! ... Calls sqvc_chi0_sqvc_dv to epsilon|dv>
  !
  USE kinds,            ONLY : DP
  USE constants,        ONLY : pi
  USE io_global,            ONLY : stdout
  USE gvect,                ONLY : gstart
  use wvfct,                only : npwx
  USE lc_variables,         ONLY : naux, veh, naux_from_file, eigenval_eps
  use cell_base,            only : omega
  USE klist,                ONLY : ngk
  use control_flags,        only : gamma_only
  USE mp,                ONLY : mp_sum
  USE mp_global,            ONLY : inter_pool_comm, intra_bgrp_comm
  !
  IMPLICIT NONE
  !
  ! ... I/O variables
  !
  REAL(DP) :: e(naux)        ! estimated eigenvalues
  REAL(DP) :: avg_iter       ! average number of iteration per mode
  !
  ! ... local variables
  !
  INTEGER, PARAMETER :: maxter = 300
  INTEGER                  :: i, j, m, iter, moved, ig
  REAL(DP), ALLOCATABLE    :: precondition(:)
  COMPLEX(DP), ALLOCATABLE :: hpsi(:), spsi(:), lagrange(:), &
                              g(:), cg(:), scg(:), ppsi(:), g0(:)  
  REAL(DP)                 :: psi_norm, a0, b0, gg0, gamma, gg, gg1, &
                              cg0, e0, es(2)
  REAL(DP)                 :: theta, cost, sint, cos2t, sin2t
  LOGICAL                  :: reorder
  INTEGER                  :: npwq02, notconv
  REAL(DP)                 :: ethr_m
  REAL(DP)                 :: thr_ec=1.D-4     ! threshold for iterative diagonalization of KS Resp. Funct.
  !
  ! ... external functions
  !
  REAL (DP), EXTERNAL :: DDOT, rndm
  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  CALL start_clock( 'lc_cg_solver' )
  !
  WRITE(stdout,'(7x,''Conjugate gradient diagonalization'')')
  !
  !
  ALLOCATE( spsi( npwx ) )
  ALLOCATE( scg(  npwx ) )
  ALLOCATE( hpsi( npwx ) )
  ALLOCATE( g(    npwx ) )
  ALLOCATE( cg(   npwx ) )
  ALLOCATE( g0(   npwx ) )
  ALLOCATE( ppsi( npwx ) )
  !    
  ALLOCATE( lagrange( naux ) )
  ALLOCATE( precondition( npwx ) )
  precondition(:) = 1.d0
  !
  avg_iter = 0.D0
  notconv  = 0
  moved    = 0
  !
  IF (naux_from_file>0) e(1:naux_from_file)=eigenval_eps(1:naux_from_file)
  !
  DO i = naux_from_file+1, naux
     DO ig = 1, ngk(1)
        veh(ig,i) = CMPLX(rndm(), rndm())
     END DO
     if (gstart==2) veh(1,i) = (0.D0,0.0d0)
  ENDDO
  !
  ! ... every eigenfunction is calculated separately
  !
  DO m = naux_from_file+1, naux
     !
     ethr_m = thr_ec
     !
     spsi     = ZERO
     scg      = ZERO
     hpsi     = ZERO
     g        = ZERO
     cg       = ZERO
     g0       = ZERO
     ppsi     = ZERO
     lagrange = ZERO
     !
     ! ... calculate S|psi>
     !
     spsi(:) = veh(:,m)
     !
     ! ... orthogonalize starting eigenfunction to those already calculated
     !     (note that orthogonalization is with overlap matrix S)
     !
     CALL ZGEMV( 'C', ngk(1), m, ONE, veh, ngk(1), spsi, 1, ZERO, lagrange, 1 )
     lagrange(1:m)=cmplx(2.D0*dble(lagrange(1:m)),0.D0)
     if (gstart==2) lagrange(1:m)=lagrange(1:m) - dble(veh(1,1:m)*spsi(1))
     !
     call mp_sum(lagrange,intra_bgrp_comm)
     !
     psi_norm = lagrange(m) 
     !
     DO j = 1, m - 1
        !
        veh(:,m)  = veh(:,m) - lagrange(j) * veh(:,j)
        !
        psi_norm = psi_norm - &
                   ( DBLE( lagrange(j) )**2 + AIMAG( lagrange(j) )**2 )
        !
     END DO
     !
     psi_norm = SQRT( psi_norm )
     !
     veh(:,m) = veh(:,m) / psi_norm

     !
     ! ... calculate starting gradient (|hpsi> = H|psi>) ...
     !
     hpsi(:)=veh(:,m)
     CALL apply_eps_kin( hpsi )
     spsi(:) = veh(:,m)
     !
     ! ... and starting eigenvalue (e = <y|PHP|y> = <psi|H|psi>)
     !
     ! ... NB:  DDOT(2*ndim,a,1,b,1) = REAL( ZDOTC(ndim,a,1,b,1) )
     !
     e(m) = 2.D0 * DDOT( 2*ngk(1), veh(1,m), 1, hpsi(1), 1 )
     if (gstart==2) e(m)=e(m)-veh(1,m)*hpsi(1) 
     !
     call mp_sum(e(m),intra_bgrp_comm)
     !
     ! ... start iteration for this band
     !
     iterate: DO iter = 1, maxter
        !
        ! ... calculate  P (PHP)|y>
        ! ... ( P = preconditioning matrix, assumed diagonal )
        !
        g(:)    = hpsi(:) / precondition(:)
        ppsi(:) = spsi(:) / precondition(:)
        !
        ! ... ppsi is now S P(P^2)|y> = S P^2|psi>)
        !
        es(1) = 2.D0 * DDOT( 2*ngk(1), spsi(1), 1, g(1), 1 )
        if (gamma_only .and. gstart==2) es(1) = es(1) - spsi(1)*g(1)
        es(2) = 2.D0 * DDOT( 2*ngk(1), spsi(1), 1, ppsi(1), 1 )
        if (gamma_only .and. gstart==2) es(2) = es(2) - spsi(1)*ppsi(1)
        !
        call mp_sum(es,intra_bgrp_comm)
        !
        es(1) = es(1) / es(2)
        !
        g(:) = g(:) - es(1) * ppsi(:)
        !
        ! ... e1 = <y| S P^2 PHP|y> / <y| S S P^2|y> ensures that 
        ! ... <g| S P^2|y> = 0
        ! ... orthogonalize to lowest eigenfunctions (already calculated)
        !
        ! ... scg is used as workspace
        !
        scg(:) = g(:)
        !
        CALL ZGEMV( 'C', ngk(1), ( m - 1 ), ONE, veh, &
                    ngk(1), scg, 1, ZERO, lagrange, 1  )
        lagrange(1:(m-1))=cmplx(2.D0*dble(lagrange(1:(m-1))),0.D0)
        if (gstart==2 .and. m>1) lagrange(1:(m-1))=lagrange(1:(m-1)) - dble(veh(1,1:(m-1))*scg(1))
        !
        call mp_sum(lagrange,intra_bgrp_comm)
        !
        DO j = 1, ( m - 1 )
           !
           g(:)   = g(:)   - lagrange(j) * veh(:,j)
           scg(:) = scg(:) - lagrange(j) * veh(:,j)
           !
        END DO
        !
        IF ( iter /= 1 ) THEN
           !
           ! ... gg1 is <g(n+1)|S|g(n)> (used in Polak-Ribiere formula)
           !
           gg1 = 2.D0 * DDOT( 2*ngk(1), g(1), 1, g0(1), 1 )
           if (gamma_only .and. gstart==2) gg1 = gg1 - g(1)*g0(1)
           !
           call mp_sum(gg1,intra_bgrp_comm)
           !
        END IF
        !
        ! ... gg is <g(n+1)|S|g(n+1)>
        !
        g0(:) = scg(:)
        !
        g0(:) = g0(:) * precondition(:)
        !
        gg = 2.D0 * DDOT( 2*ngk(1), g(1), 1, g0(1), 1 )
        if (gamma_only .and. gstart==2) gg = gg - g(1)*g0(1)
        !
        call mp_sum(gg,intra_bgrp_comm)
        !
        IF ( iter == 1 ) THEN
           !
           ! ... starting iteration, the conjugate gradient |cg> = |g>
           !
           gg0 = gg
           !
           cg(:) = g(:)
           !
        ELSE
           !
           ! ... |cg(n+1)> = |g(n+1)> + gamma(n) * |cg(n)>
           !
           ! ... Polak-Ribiere formula :
           !
           gamma = ( gg - gg1 ) / gg0
           gg0   = gg
           !
           cg(:) = cg(:) * gamma
           cg(:) = g(:) + cg(:)
           !
           ! ... The following is needed because <y(n+1)| S P^2 |cg(n+1)> 
           ! ... is not 0. In fact :
           ! ... <y(n+1)| S P^2 |cg(n)> = sin(theta)*<cg(n)|S|cg(n)>
           !
           psi_norm = gamma * cg0 * sint
           !
           cg(:) = cg(:) - psi_norm * veh(:,m)
           !
        END IF
        !
        ! ... |cg> contains now the conjugate gradient
        !
        ! ... |scg> is S|cg>
        !
        ppsi(:)=cg(:)
        CALL apply_eps_kin( ppsi )
        scg(:) = cg(:)
        !
        cg0 = 2.D0*DDOT( 2*ngk(1), cg(1), 1, scg(1), 1 )
        if (gamma_only .and. gstart==2) cg0 = cg0 - cg(1)*scg(1)
        !
        call mp_sum(cg0,intra_bgrp_comm)
        !
        cg0 = SQRT( cg0 )
        !
        ! ... |ppsi> contains now HP|cg>
        ! ... minimize <y(t)|PHP|y(t)> , where :
        ! ...                         |y(t)> = cos(t)|y> + sin(t)/cg0 |cg>
        ! ... Note that  <y|P^2S|y> = 1, <y|P^2S|cg> = 0 ,
        ! ...           <cg|P^2S|cg> = cg0^2
        ! ... so that the result is correctly normalized :
        ! ...                           <y(t)|P^2S|y(t)> = 1
        !
        a0 = 2.D0 * DDOT( 2*ngk(1), veh(1,m), 1, ppsi(1), 1 ) 
        if (gamma_only .and. gstart==2) a0 = a0 - veh(1,m)*ppsi(1)
        a0 = 2.D0 * a0 / cg0
        !
        call mp_sum(a0,intra_bgrp_comm)
        !
        b0 = 2.D0 * DDOT( 2*ngk(1), cg(1), 1, ppsi(1), 1 ) 
        if (gamma_only .and. gstart==2) b0 = b0 - cg(1)*ppsi(1)
        b0 = b0 / cg0**2
        !
        call mp_sum(b0,intra_bgrp_comm)
        !
        e0 = e(m)
        !
        theta = 0.5D0 * ATAN( a0 / ( e0 - b0 ) )
        !
        cost = COS( theta )
        sint = SIN( theta )
        !
        cos2t = cost*cost - sint*sint
        sin2t = 2.D0*cost*sint
        !
        es(1) = 0.5D0 * (   ( e0 - b0 ) * cos2t + a0 * sin2t + e0 + b0 )
        es(2) = 0.5D0 * ( - ( e0 - b0 ) * cos2t - a0 * sin2t + e0 + b0 )
        !
        ! ... there are two possible solutions, choose the minimum
        !
        IF ( es(2) < es(1) ) THEN
           !
           theta = theta + 0.5D0 * pi
           !
           cost = COS( theta )
           sint = SIN( theta )
           !
        END IF
        !
        ! ... new estimate of the eigenvalue
        !
        e(m) = MIN( es(1), es(2) )
        write(stdout,*) "eig=",e(m)*(4.D0/omega)
        !
        ! ... upgrade |psi>
        !
        veh(:,m) = cost * veh(:,m) + sint / cg0 * cg(:)
        !
        ! ... here one could test convergence on the energy
        !
        IF ( ABS( e(m) - e0 )*4.D0/omega < ethr_m ) write(stdout,*) "CONVERGED EIG", e(m)*(4.D0/omega),"(# iterations",iter,")"
        IF ( ABS( e(m) - e0 )*4.D0/omega > ethr_m .and. iter==maxter ) write(stdout,*) "NOT CONVERGED EIG",iter, e(m),e0
        IF ( ABS( e(m) - e0 )*4.D0/omega < ethr_m ) write(stdout,*) 
        IF ( ABS( e(m) - e0 )*4.D0/omega < ethr_m ) EXIT iterate
        !
        ! ... upgrade H|psi> and S|psi>
        !
        spsi(:) = cost * spsi(:) + sint / cg0 * scg(:)
        !
        hpsi(:) = cost * hpsi(:) + sint / cg0 * ppsi(:)
        !
     END DO iterate
     !
     IF ( iter >= maxter ) notconv = notconv + 1
     !
     avg_iter = avg_iter + DBLE(iter+1)
     !
     ! ... reorder eigenvalues if they are not in the right order
     ! ... ( this CAN and WILL happen in not-so-special cases )
     !
     reorder = .TRUE.
     IF ( m > 1 .AND. reorder ) THEN
        !
        IF ( (e(m) - e(m-1))*4.D0/omega < - 2.D0 * ethr_m ) THEN
           !
           ! ... if the last calculated eigenvalue is not the largest...
           !
           DO i = m - 2, 1, - 1
              !
              IF ( (e(m) - e(i))*4.D0/omega > 2.D0 * ethr_m ) EXIT
              !
           END DO
           !
           i = i + 1
           !
           moved = moved + 1
           !
           ! ... last calculated eigenvalue should be in the 
           ! ... i-th position: reorder
           !
           e0 = e(m)
           !
           ppsi(:) = veh(:,m)
           !
           DO j = m, i + 1, - 1
              !
              e(j) = e(j-1)
              !
              veh(:,j) = veh(:,j-1)
              !
           END DO
           !
           e(i) = e0
           !
           veh(:,i) = ppsi(:)
           !
           ! ... this procedure should be good if only a few inversions occur,
           ! ... extremely inefficient if eigenvectors are often in bad order
           ! ... ( but this should not happen )
           !
        END IF
        !
     END IF
     !
  END DO
  !
  avg_iter = avg_iter / DBLE(naux-naux_from_file)
  write(stdout,*)
  write(stdout,*) "average # iterations",avg_iter
  write(stdout,*)
  DO m = 1, naux
     write(stdout,*) "FINAL EIGENVALUE",e(m)*4.d0/omega
  END DO
  eigenval_eps=e
  !
  DEALLOCATE( lagrange )
  DEALLOCATE( ppsi )
  DEALLOCATE( g0 )
  DEALLOCATE( cg )
  DEALLOCATE( g )
  DEALLOCATE( hpsi )
  DEALLOCATE( scg )
  DEALLOCATE( spsi )
  !
  CALL stop_clock( 'lc_cg_solver' )
  !
  RETURN
  !
END SUBROUTINE lc_cg_solver
!
    !------------------------------------------------------------------------
    function rndm()
      !------------------------------------------------------------------------
      !
      ! ... random number generator equivalent to ran1 of Num.Rec.
      !
      USE kinds,            ONLY : DP
      IMPLICIT NONE
      !
      REAL(DP) :: rndm
      REAL(DP) :: shuffle(32)
      INTEGER  :: i
      LOGICAL  :: first
      DATA first / .TRUE. /
      SAVE first, shuffle, i
      INTEGER, SAVE :: irand
      REAL(DP), EXTERNAL :: rndx
      !
      ! ... starting seed, must be not be 0
      !
      IF ( first ) irand = -1
      !
      IF ( first .OR. irand < 0 ) THEN
         !
         irand = - irand
         !
         DO i = 32 + 8, 1, - 1
            !
            shuffle( MIN( i, 32 ) ) = rndx( irand )
            !
         END DO
         !
         i = 32 * shuffle(1) + 1
         !
         first = .FALSE.
         !
      end if
      !
      rndm = shuffle(i)
      !
      shuffle(i) = rndx( irand )
      !
      i = 32 * rndm + 1
      !
      RETURN
      !
    END FUNCTION rndm
    !
    !------------------------------------------------------------------------
    FUNCTION rndx( irand )
      !------------------------------------------------------------------------
      !
      ! ... random number generator equivalent to ran0 of Num.Rec.
      !
      USE kinds,            ONLY : DP
      IMPLICIT NONE
      !
      INTEGER, INTENT(INOUT) :: irand
      REAL(DP)               :: rndx
      !
      INTEGER  :: im, ia, iq, ir, is, it
      REAL(DP) :: obm
      LOGICAL  :: first
      DATA first / .TRUE. /
      SAVE im, ia, iq, ir, obm, first
      !
      IF ( first ) THEN
         !
         ! ... this is 2**31-1 avoiding overflow
         !
         im  = 2 * ( 2**30 - 1 ) + 1
         obm = 1.0 / im
         ia  = 7*7*7*7*7
         iq  = im / ia
         ir  = im - ia * iq
         !
         first = .FALSE.
         !
      END IF
      !
      is = irand / iq
      it = irand - is * iq
      !
      irand = ia * it - is * ir
      !
      IF ( irand < 0 ) irand = irand + im
      !
      rndx = irand * obm
      !
      RETURN
      !
    END FUNCTION rndx
